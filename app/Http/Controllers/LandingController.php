<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;

class LandingController extends Controller
{

    public function index() {

      $plans = Plan::orderBy('monthly_price', 'DESC')->get();

      return view('landing-page',
        ['plans'     => $plans]
      );

    }
}
