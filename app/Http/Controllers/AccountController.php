<?php

namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;

class AccountController extends BaseController
{

    public function index() {

    }

    public function settings() {

    }

    public function organization() {
      return view('account/organization',[
        'currentUser'       => $this->currentUser(),
        'candidate'         => null,
        'candidateSources'  => ['individual' => 'Individual', 'company' => 'Company']
      ]);
    }

    public function payments() {

    }

}
