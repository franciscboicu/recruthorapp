<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Partner;

class PartnerController extends BaseController
{
    public function browse() 
    {
        // $vendors = Vendor::organization()->get();
        $partners = Partner::organization()->get();

        // dd($vendors->first()->name);
    }

    public function create()
    {

    }

    public function save()
    {

    }

    public function edit()
    {

    }
}
