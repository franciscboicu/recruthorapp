<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use Auth;
use View;
use App\Candidate;
use App\Activity;
use App\CandidateProject;
use App\File;

class CandidateController extends BaseController
{

  public function browse(Request $request) {

    $candidates = Candidate::getMyCandidates($this->currentUser());
    $candidatesCount = $candidates->count();

    return view('candidates.candidates-browse',[
      'candidates'        => $candidates,
      'candidatesCount'   => $candidatesCount,
      'currentUser'       => $this->currentUser(),
    ]);
  }

  public function read(Request $request, $id) {

    $candidate = Candidate::find($id);

    $activities = Activity::with('user')
                          ->where('entity_class', 'App\Candidate')
                          ->where('entity_id', $candidate->id)
                          ->orderBy('id', 'DESC')
                          ->get();

    $candidateProjects = CandidateProject::with('project')->where('candidate_id', $id)->get();

    return view('candidates.candidates-read', [
        'currentUser'         => $this->currentUser(),
        'activities'          => $activities,
        'candidate'           => $candidate,
        'candidateProjects'   => $candidateProjects,
    ]);

  }

  public function edit(Request $request, $id, $section = 'basic-info' ) {

    $candidate = Candidate::find($id);

    if ($request->isMethod('post')) {
      $candidate->update($request->except('_token'));
    }

    /** make use of subsections !TODO! Find a clearer solution */
    if (isset($_GET['sub'])) {

      if ($_GET['sub'] == 'jobs') {
        $candidateProjects = CandidateProject::with('project')->where('candidate_id', $id)->get();
        View::share('candidateProjects', $candidateProjects);
      }

      if ($_GET['sub'] == 'docs') {
        $candidateDocs = File::getCVs($candidate->id);
        View::share('candidateDocs', $candidateDocs);
      }

    }

    return view('candidates.candidates-add-new',[
      'candidate'           => $candidate,
      'candidateSources'    => Candidate::getCandidateSources(),
      'currentUser'         => $this->currentUser()
    ]);

  }

  public function add(Request $request) {

    if ($request->isMethod('post')) {
      /** add to request to be able to push the request data in Eloquent */
      $request->request->add([
                            'added_by_id' => $this->currentUser()->id,
                            'organization_id' => $this->currentUser()->organization_id
                            ]);

      $candidate = Candidate::create($request->except('_token'));
      // dd($this->currentUser()->organization_id);
      $request['languages'] = 'ab,bc';
      // dd($request->except('_token'));
      if ($candidate) {
        Activity::pushActivity('candidate-new', $candidate, $this->currentUser());
        return redirect(route('candidates/edit', ['id' => $candidate->id]));
      }

    }

    return view('candidates.candidates-add-new',[
        'candidateSources'    => Candidate::getCandidateSources(),
        'currentUser'         => $this->currentUser()
    ]);
  }

  public function delete(Request $request, $id) {
    return Candidate::find($id)->delete();
  }

  /**
   *  Upload of CV for Candidate
   * If database upload successful also
   * treat the file on the disk
   */
  public function fileUploadPost(Request $request)
  {
    $request->validate(File::basicValidationRules);

    if ( 'pdf' !== strtolower ( request()->file->getClientOriginalExtension() ) ) {
      return response()->json( [ 'errors' => ['Your file is not a PDF.'] ]);
    }

    $fileName = time() . "-" . md5($this->currentUser()->id . "-" . $this->currentUser()->organization_id) . "-" . $this->currentUser()->id . "-" . $this->currentUser()->organization_id . "." . request()->file->getClientOriginalExtension();

    try {

      $dbFile = File::uploadCV($request, $fileName);

      if ($dbFile) {
          $uploadFile = request()->file->move(public_path('files'), $fileName);
          return response()->json(['success' => 'You have successfully upload file.']);
        }

        return response()->json(['errors' => ['DB File Error']]);

    } catch (Exception $ex) {

      return response()->json(['errors' => [ $ex->message() ]]);

    }
      return response()->json(['errors' => ['DB File Error']]);
  }




}
