<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Project;
use App\Partner;
use App\Activity;
use App\CandidateProject;
use App\Candidate;
use App\ProjectNote;
use App\Organization;

class ProjectController extends BaseController
{
  /** Show projects belonging to current organization, current user **/
  public function browse(Request $request) {

    $projects      = Project::with('partner', 'candidates')->organization()->currentUser()->get();
    $projectsCount = $projects->count();

    return view('projects.projects-browse', [
      'projects'            => $projects,
      'projectsCount'       => $projectsCount,
      'currentUser'         => $this->currentUser()
    ]);

  }

  /** Show a selected Project **/
  public function read(Request $request, $id) {

    $project = Project::with('partner', 'candidates')
                        ->organization()
                        ->currentUser()
                        ->where('id', $id)
                        ->first();

    $projectCandidates = CandidateProject::with('candidate')
                          ->where('project_id', $project->id)
                          ->get();


    /** ToDo: Refactor */
    $projectFacilities = Project::facilities;
    return view('projects.projects-read',[
        'currentUser'         => $this->currentUser(),
        'project'             => $project,
        'projectCandidates'   => $projectCandidates,
        'flow'                => Project::getFlow(),
        'projectFacilities'  => $projectFacilities,
    ]);
  }

  /**
   * Change where a candidate is found in the pipeline
   * This is a walk through some statuses
   */
  public function changeCandidatePipelineState(Request $request){

    if ('positive' === $request->get('change_how')) {

      $candidateOnProject = CandidateProject::where('project_id', $request->get('project_id'))
                                            ->where('candidate_id', $request->get('candidate_id'))
                                            ->first();

      if (!$candidateOnProject) {
        return response()->json(['success' => false, 'message' => 'Invalid data. Candidate not found!']);
      }

      if ('applied' === $candidateOnProject->stage) {
        $candidateOnProject->update(['stage' => 'phone-screen']);
        // Activity::pushActivity('candidate-pipe-progress', $candidateOnProject, $this->currentUser(), [], Project::find($candidateOnProject->project_id));
        return response()->json(['success'=>true,'message' => '', 'refres' => true, 'redirect' => false]);
      }

      return response()->json($request);

    }

    return response()->json('LESS');
  }

  /**
   * Read the pipeline and intersect with candidates to
   * display them on columns
   */
  public function readPipeline(Request $request, $id) {

    $project = Project::with('partner', 'candidates')
                      ->organization()
                      ->currentUser()
                      ->first();

    $projectCandidates = CandidateProject::where('project_id', $project->id)
                                          ->get();

    return view('projects.projects-read-pipeline',[
      'currentUser'       => $this->currentUser(),
      'project'           => $project,
      'projectCandidates' => $projectCandidates
    ]);
  }

  public function edit(Request $request, $id) {

    $project = Project::organization()->currentUser()->where('id', $id)->first();

    if ($request->isMethod('post')) {
      $project->update($request->except('_token'));
    }

    $staticAttributes  = Project::getAllStaticAttributes();
    $projectFacilities = Project::facilities;

    $companies         = Partner::organization()->get();

    return view('projects.projects-add',[
      'project'             => $project,
      'educations'          => $staticAttributes['educations'],
      'xpLevels'            => $staticAttributes['xpLevels'],
      'industries'          => $staticAttributes['industries'],
      'types'               => $staticAttributes['types'],
      'departments'         => $staticAttributes['departments'],
      'projectFacilities'   => $projectFacilities,
      'companies'           => $companies,
      'currentUser'         => $this->currentUser(),
    ]);
  }


  public function add(Request $request) {

    if ($request->isMethod('post')) {

      $request->request->add(['owner_id' => $this->currentUser()->id, 'organization_id' => $this->currentUser()->organization_id]);
      $project = Project::create($request->except('_token'));

      if ($project) {
        Activity::pushActivity('project-new', $project, $this->currentUser());
        return redirect(route('projects/edit', ['id' => $project->id]));
      }

    }

    $projectFacilities = Project::facilities;
    $staticAttributes = Project::getAllStaticAttributes();
    $companies        = Partner::organization()->get();


    return view('projects.projects-add', [
      'educations'          => $staticAttributes['educations'],
      'xpLevels'            => $staticAttributes['xpLevels'],
      'industries'          => $staticAttributes['industries'],
      'types'               => $staticAttributes['types'],
      'departments'         => $staticAttributes['departments'],
      'projectFacilities'   => $projectFacilities,
      'companies'           => $companies,
      'currentUser'         => $this->currentUser(),
    ]);

  }

  public function delete(Request $request, $id) {

  }

  public function getCandidatesForProject(Request $request, $id) {

    $project = Project::organization()->currentUser()->where('id', $id)->first();

    $projectMainSkills = $project->getMainSkillsList();

    $projectCandidates = CandidateProject::with('candidate')->where('project_id', $project->id)->get();

    $candidates = Candidate::getMyCandidates($this->currentUser());

    return view('projects.candidates-to-project-content',[
      'candidates'    => $candidates,
      'project'       => $project
    ]);
  }

  /**
   * Take the info of candidate, related to the project
   */
  public function candidateInfo(Request $request, $projectId, $candidateId){

    $candidateProject = CandidateProject::where('project_id', $projectId)
                                            ->where('candidate_id', $candidateId)
                                            ->firstOrFail();

    $candidate        = Candidate::find($candidateProject->candidate_id)->first();

    $project          = Project::find($candidateProject->project_id);

    $projectNotes     = ProjectNote::where('candidate_id', $candidateProject->candidate_id)
                                        ->where('project_id', $candidateProject->project_id)
                                        ->orderBy('id', 'DESC')
                                        ->get();

    return view('projects.candidate-info-popup-content', [
      'projectNotes'        => $projectNotes,
      'candidate'           => $candidate,
      'project'             => $project,
      'candidateProject'    => $candidateProject,
      'currentUser'         => $this->currentUser()
    ]);
  }

  /**
   * Add a note about Candidate, related to Recruiter interaction
   * Notes are kept per project and can be all seen on Candidate page
   */
  public function addNote(Request $request) {

    if (!$request->get('note')) {
      return response()->json(['success' => false,'message' => 'Invalid data! Please add a note']);
    }

    $request->request->add(['added_by_user_id' => $this->currentUser()->id,
                            'added_by_name' => $this->currentUser()->name,
                            'organization_id' => $this->currentUser()->organization_id
                            ]);

    $data = $request->except('_token');

    $projectNote =  ProjectNote::create($data);

    if (!$projectNote) {
      return response()->json(['success' => false,'message' => 'Something went wrong!']);
    }

    return response()->json(['success' => true,'message' => '']);
  }

  /**
   * @param  Request $request [description]
   * @return @json
   */
  public function deleteNote(Request $request) {

    if ( ! $request->get('note_id') ) {
      return response()->json(['success' => false,'message' => 'Invalid data! Please add a note']);
    }

    $projectNote = ProjectNote::organization()->where('id', $request->get('note_id'))->first();

    if (!$projectNote->delete()) {
      return response()->json(['success' => false,'message' => 'Something went wrong!']);
    }

    return response()->json(['success' => true,'message' => '']);

  }

  public function updateStage(Request $request) {

    $candidateProject = CandidateProject::where('project_id', $request->get('project_id'))
                                            ->where('candidate_id', $request->get('candidate_id'))
                                            ->where('stage', "!=", $request->get('stage'))
                                            ->first();

    if ($candidateProject) {
      $candidateProject->setStage($request->get('stage'));
      $candidateProject->save();
      return response()->json(['success' => true,'message' => '', 'refreshOnPopupClose' => true]);
    }

    return response()->json(['success' => false,'message' => 'Invalid data! 2']);
  }

}
