<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Auth;

use App\Http\Helpers\Check;
use App\Project;
use App\Candidate;
use App\Activity;
use App\Services\CandidateProjectMatchingService;

class HomeController extends BaseController
{
    public function index()
    {
        return view('home');
    }

    public function dashboard()
    {
      $projectsCount    = Project::where('organization_id', $this->currentUser()
                                    ->organization_id)->count();
      $candidatesCount  = Candidate::where('organization_id', $this->currentUser()
                                    ->organization_id)->count();
      $latestActivity   = Activity::where('organization_id', $this->currentUser()
                                    ->organization_id)->orderBy('id', 'DESC')->get();

      return view('dashboard', [
        'currentUser'           => $this->currentUser(),
        'projectsCount'         => $projectsCount,
        'candidatesCount'       => $candidatesCount,
        'latestActivity'        => $latestActivity,
      ]);
    }
}
