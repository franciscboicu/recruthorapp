<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use Auth;
use App\File;

class FileController extends BaseController
{
    public function remove(Request $request){

      if (!$this->currentUser()) {
        return response()->json(['success' => false, 'message' => 'Hey! Where\'s the session?']);
      }

      $removedFile = File::remove($request);

      if ($removedFile) {
        return response()->json(['success' => true, 'message' => $request->get('filename') . ' was removed!']);
      }

      return response()->json(['success' => false, 'message' => $request->get('filename') . ' was not removed!']);
    }
}
