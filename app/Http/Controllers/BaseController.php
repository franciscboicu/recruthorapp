<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use View;
use App\Organization;

class BaseController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        View::share('currentUser', $this->currentUser());
        View::share('currencies', $this->currencies());
    }

    public function currentUser(){
      return Auth::user();
    }

    public function currencies(){
      return Organization::currencies;
    }


}
