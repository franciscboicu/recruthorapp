<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CandidateProject extends Model
{
    protected $guarded = ['id'];
    protected $table="candidate_project";

    public function candidate(){
      return $this->belongsTo('App\Candidate');
    }

    public function project(){
      return $this->belongsTo('App\Project')->with('vendor');
    }

    /** Check if a user belongs to a stage */
    public function is($stage) {
      return $stage === $this->stage;
    }

    public function setStage($stage) {
      $this->stage = $stage;
    }
}
