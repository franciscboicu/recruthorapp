<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Partner extends Model
{
  protected $table = 'partners';

  public function scopeOrganization($query) {
      return $query->where('organization_id', Auth::user()->organization_id);
  }

  public function projects(){
  	return $this->hasMany('App\Project');
  }

}
