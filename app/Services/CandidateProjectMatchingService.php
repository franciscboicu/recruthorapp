<?php

namespace App\Services;
use App\Project;

class CandidateProjectMatchingService
{
    protected $project = null;

    public function __construct(Project $project) {
        $this->project = $project;
    }

    public function doMatching()
    {
        return $this->project;
    }

}
