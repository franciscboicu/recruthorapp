<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table = 'organizations';

    const currencies = ['USD', 'RON', 'EUR'];
}
