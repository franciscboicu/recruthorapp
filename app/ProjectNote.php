<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProjectNote extends Model
{
  protected $guarded = ['id'];

  public function scopeOrganization($query) {
      return $query->where('organization_id', Auth::user()->organization_id);
  }

  public function scopeCurrentUser($query) {
      return $query->where('owner_id', Auth::id());
  }

  public function user(){
    return $this->hasMany('App\User', 'id', 'added_by_user_id');
  }



}
