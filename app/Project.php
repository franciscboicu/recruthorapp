<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Project extends Model
{
    protected $table = 'projects';
    protected $guarded = ['id'];

    const facilities = [
      'f_meal_tickets'  => [
        'label'     => 'Meal Tickets',
        'description' => ''
      ],
      'f_coffee'  => [
        'label'     => 'Free Coffee/Tea/Drinks',
        'description' => ''
      ],
      'f_relaxation'  => [
        'label'     => 'Meal Tickets',
        'description' => ''
      ],
      'f_gym'  => [
        'label'     => 'Gym Vouchers',
        'description' => ''
      ],
      'f_teambuild'  => [
        'label'     => 'Teambuildings',
        'description' => ''
      ],
      'f_flexible'  => [
        'label'     => 'Flexible Working Time',
        'description' => ''
      ],'f_insurance'  => [
        'label'     => 'Health Insurance',
        'description' => ''
      ],
      'f_courses'  => [
        'label'     => 'Subscription to Courses',
        'description' => ''
      ],
      'f_relocation'  => [
        'label'     => 'Relocation Help',
        'description' => ''
      ],
      'f_parking'  => [
        'label'     => 'Parking',
        'description' => ''
      ],

    ];

    public function candidates(){
      return $this->belongsToMany(Candidate::class, 'candidate_project')->withPivot('stage');
    }

    public function partner(){
      return $this->hasOne('App\Partner', 'id', 'partner_id');
    }

    /** Scoped for current Organization */
    public function scopeOrganization($query) {
        return $query->where('organization_id', Auth::user()->organization_id);
    }

    /** Scoped for current user */
    public function scopeCurrentUser($query) {
        return $query->where('owner_id', Auth::id());
    }

    /**
     * Get the project department types
     * !TODO! Move to database and allow dynamic
     */
    public static function getDepartments(){
      return [
        'product-development'    => 'Product Development',
        'management'  => 'Management'
      ];
    }

   /**
     * Get the project job types
     * !TODO! Move to database and allow dynamic
     */
    public static function getTypes(){
      return [
        'project-based-office'    => 'Project Based (in Office)',
        'project-based-remote'    => 'Project Based (remote)',
        'full-time'               => 'Full time',
        'part-time'               => 'Part time',
        'internship'              => 'Internship'
      ];
    }

    /**
     * Get the industries for jobs
     * !TODO! Move to database and allow dynamic
     */
    public static function getIndustries(){
      return [
        'accounting'              => 'Accounting',
        'software'                => 'Software',
        'banking'                 => 'Banking',
        'finance'                 => 'Finance',
        'crypto'                  => 'Crypto',
        'gaming'                  => 'Gaming',
        'security'                => 'Security',
      ];
    }

   /**
     * Get the education level for jobs
     * !TODO! Move to database and allow dynamic
     */
    public static function getEducations(){
      return [
        'highschool-grad'         => 'Highschool Graduate',
        'highschool-equivalent'   => 'Highschool Equivalent',
        'licensed'                => 'Licensed',
        'bachelors-degree'        => 'Bachelors Degree',
        'phd'                     => 'PHD Doctorate',
        'work-experience'         => 'Work Experience',
      ];
    }

   /**
     * Get the experience level for jobs
     * !TODO! Move to database and allow dynamic
     */
    public static function experienceLevels(){
      return [
        'junior'                  => 'Junior',
        'junior-middle'           => 'Junior to Middle',
        'middle'                  => 'Middle',
        'middle-senior'           => 'Middle to Senior',
        'senior'                  => 'Senior',
      ];
    }

    /**
     * Get the Pipeline for the Project
     * !TODO! Make general pipeline on signup
     * and allow the account manager to change the
     * pipe titles
     * ----------------
     * Return all or by key
     */
    public static function getFlow($what = null){
      $return = [
        'applied'                   => ['label' => 'Applied', 'icon' => ''],
        'phone-screen'              => ['label' => 'Phone Screen', 'icon' => ''],
        'interview'                 => ['label' => 'Interview', 'icon' => ''],
        'evaluation'                => ['label' => 'Evaluation', 'icon' => ''],
        'offer'                     => ['label' => 'Offer', 'icon' => ''],
        'closed'                    => ['label' => 'Closed', 'icon' => ''],
      ];

      return ($what) ? $return[$what]: $return;
    }

    /**
     *  get Short Country by extracting first two letters from string
     * !TODO! Move to database, create relations and add select with search
     * on frontend
     *
    */
    public function getShortCountry() {
      return ($this->country_name !== "") ? substr($this->country_name, 0, 2): "-";
    }

    /**
     * Same as for country
     */
    public function getShortCity(){
      return ($this->city_name !== "") ? substr($this->city_name, 0, 4): "-";
    }

    public static function getAllStaticAttributes()
    {
      return [
        'departments'     => self::getDepartments(),
        'types'           => self::getTypes(),
        'industries'      => self::getIndustries(),
        'educations'      => self::getEducations(),
        'xpLevels'        => self::experienceLevels(),
      ];
    }


    public function getMainSkillsList() {
      return (isset($this->main_skills)) ? explode(",", $this->main_skills): null;
    }

}
