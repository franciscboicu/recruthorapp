<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'provider', 'provider_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function organization(){
      return $this->hasOne('App\Organization', 'id', 'owner_id');
    }

    public function activities(){
      return $this->hasMany('App\Activity', 'id', 'owner_id');
    }

    public function getName(){
      return $this->name;
    }

    public function getEmail(){
      return $this->email;
    }

    public function getTourStage(){
      return $this->tour_stage;
    }

    public function isOwner(){
      return $this->is_owner;
    }
}
