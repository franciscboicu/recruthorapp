<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $table = "plans";

    /** Button css class according to plan type */
    const BUTTONS = [
      'free'        => 'btn-default',
      'solo'        => 'btn-success',
      'team'        => 'btn-info'
    ];

    /** Plan panel css class according to plan type */
    const PANELS = [
      'free'        => 'panel-grey',
      'solo'        => 'panel-green',
      'team'        => 'panel-blue'
    ];
}
