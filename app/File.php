<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class File extends Model
{
    protected $table = 'files';
    protected $guarded = ['id'];

    const basicValidationRules = [
        'file'         => 'required',
        'candidate_id' => 'required',
        'note'         => 'required'
    ];

    const namespace = [
      'candidate' => 'App\Candidate'
    ];

    public function scopeOrganization($query) {
        return $query->where('organization_id', Auth::user()->organization_id);
    }

    public function scopeLatestFirst($query){
      return $query->orderBy('id', 'DESC');
    }

    /**
     * Get Files of Namespace, either by ID either All
     * Scoped for Organization
     */
    public static function getFiles($namespace = NULL, $entityId = NULL){

      if (NULL !== $namespace && NULL === $entityId) {
        return File::organization()->where('entity_namespace', 'App\Candidate')->latestFirst()->get();
      }

      if (NULL !== $namespace && NULL !== $entityId) {
        return File::organization()->where('entity_namespace', 'App\Candidate')->latestFirst()->where('entity_id', $entityId)->get();
      }

      /** Return files of organization if both params NULL */
      return File::organization();
    }

    /**
     * Get all CV's of a candidate
     */
    public static function getCVs($candidateId) {
        return File::getFiles('App\Candidate', $candidateId);
    }

    /** Get CV's of a candidate, by fileId */
    public static function CVById($fileId) {
        return File::find($fileId);
    }

    /** Remove a file from database. Scoped for Organization */
    public static function remove($request){
      $file = File::where('filename',$request->get('filename'))->organization()->first();
      return $file->delete();
    }

    /** Do the "database upload" file */
    public static function uploadCV($request, $fileName) {
      return File::create([
        'organization_id'   => Auth::user()->organization_id,
        'user_id'           => Auth::id(),
        'type'              => 'cv',
        'entity_namespace'  => 'App\Candidate',
        'entity_id'         => $request->get('candidate_id'),
        'filename'          => $fileName,
        'note'              => $request->get('note')
      ]);
    }

}
