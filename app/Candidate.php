<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{

  protected $guarded = ["id"];

  public function projects() {
    return $this->belongsToMany(App\Project::class);
  }

  /**
   * Static types of Candidate Sources
   * !TODO!
   * Replace with dynamic cached data from database
   */
  public static function getCandidateSources(){

    $return['referenced'] = [
      'friend'    => 'by a friend',
      'collegue'  => 'by a collegue',
      'candidate' => 'by another candidate',
      'other'     => 'Other reference'
    ];

    $return['online'] = [
      'fb-group'      => 'Facebook Group',
      'linkedin'      => 'Linkedin',
      'github'        => 'GitHub',
      'other'         => 'Other online '
    ];

    $return['applied'] = [
      'recruthor'       => 'on Recruthor',
      'best-jobs'       => 'Best Jobs',
      'ejobs'           => 'EJobs',
      'other'           => 'Other'
    ];

    return $return;
  }

  /**
   * Get the candidates of a specific team member
   */
  public static function getMyCandidates($user){
    return Candidate::where('added_by_id', $user->id)->paginate(10);
  }

  /**
   * Get the Main Skills
   * Returning array by splitting string
   */
  public function getMainSkillsList($asText = false) {
    if (true == $asText) return $this->main_skills;
    return (isset($this->main_skills)) ? explode(",", $this->main_skills): null;
  }

  /**
   * Receives the project skills array
   */
  public function getOnlyMatchedSkills($projectMainSkills) {
    return array_intersect($this->getMainSkillsList(false), $projectMainSkills);
  }

  /**
   * Get the Secondary Skills
   * Returning array by splitting string
   */
  public function getSecondarySkillsList() {
    return (isset($this->secondary_skills)) ? explode(",", $this->secondary_skills): null;
  }
}
