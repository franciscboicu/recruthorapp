<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
  protected $guarded = ["id"];
  protected $table = 'activities';

  protected $activityTextFormat = [
    'candidate-pipe-progress' => '<a href="">%s</a> changed the flow for <a href="">%s</a>, in <a href="">%s</a> project',
    'candidate-new'           => '<a href="">%s</a> added a new candidate named <a href="">%s</a>',
    'project-new'             => '<a href="">%s</a> Created a new project <a href="">%s</a>',
  ];


  public function user(){
    return $this->hasOne('App\User', 'id', 'owner_id');
  }

  /**
   * Create a new activity, based on type
   * 'candidate-pipe-progress', 'candidate-new','project-new'
   * REFACTOR THIS SHIT!
   */
  public static function pushActivity($type, $entity, $user, $data=[], $extraEntity = null) {

    if ('candidate-pipe-progress' == $type) {
      Activity::create([
        'owner_id'           => $user->id,
        'organization_id'    => $user->organization_id,
        'type'               => $type,
        'entity_class'       => 'App\Candidate',
        'entity_id'          => $entity->candidate_id,
        'text'               => '<a href="">%s</a> changed the flow for <a href="">%s</a>, in <a href="">%s</a> project',
        // 'data'               => json_encode($user->name, );
      ]);
    }

    if ('candidate-new' == $type) {
      Activity::create([
        'owner_id'          =>  $user->id,
        'organization_id'   => $user->organization_id,
        'type'              => 'candidate-new',
        'entity_class'      => 'App\Candidate',
        'entity_id'         => $entity->id,
        'entity_sub_id'     => 1,
        'text'              => '<a href="">%s</a> added a new candidate named <a href="">%s</a>',
        'data'              => json_encode([$user->name, $entity->fullname]),
        'localised'         => false
      ]);
    }

    if ('project-new' == $type) {
      Activity::create([
        'owner_id'          => $user->id,
        'organization_id'   => $user->organization_id,
        'type'              => 'project-new',
        'entity_class'      => 'App\Project',
        'entity_id'         => $entity->id,
        'entity_sub_id'     => 1,
        'text'              => '<a href="">%s</a> Created a new project <a href="">%s</a>',
        'data'              => json_encode([$user->name, $entity->title]),
        'localised'         => false
      ]);
    }

  }

  /**
   * Get the text of the activity, by replacing the params
   * inside the text format
   */
  public function getActivityText(){
    $data = json_decode($this->data);
    return vsprintf($this->text, $data);
  }

  /**
   * Get the human readable "$time ago" timestamp
   */
  public function getActivityTime(){
    return \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
  }

}
