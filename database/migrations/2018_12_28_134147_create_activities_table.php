<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_id');
            $table->integer('organization_id');
            $table->string('type');
            $table->string('entity_class');
            $table->integer('entity_id');
            $table->integer('entity_sub_id');
            $table->string('text', 455);
            $table->string('data', 255)->default(NULL)->nullable();
            $table->boolean('localised')->default(0)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
