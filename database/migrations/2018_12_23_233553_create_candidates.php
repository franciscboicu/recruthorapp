<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('added_by_id')->default(NULL)->nullable();
            $table->integer('organization_id')->default(NULL)->nullable();
            $table->string('fullname', 80)->default(NULL)->nullable();
            $table->string('phone', 40)->default(NULL)->nullable();
            $table->string('email')->default(NULL)->nullable();
            $table->string('skype_id')->default(NULL)->nullable();
            $table->string('linkedin_profile')->default(NULL)->nullable();
            $table->string('country_name')->default(NULL)->nullable();
            $table->string('city_name')->default(NULL)->nullable();
            $table->integer('country_id')->default(NULL)->nullable();
            $table->integer('city_id')->default(NULL)->nullable();
            $table->boolean('relocation')->default(0);
            $table->boolean('remote')->default(0);
            $table->string('source')->default(NULL)->nullable();
            $table->text('main_skills');
            $table->text('secondary_skills');
            $table->text('languages');
            $table->integer('main_industry')->default(NULL)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
