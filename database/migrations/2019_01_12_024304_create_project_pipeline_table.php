<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectPipelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_project', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->integer('project_id');
            $table->integer('organization_id');
            $table->integer('added_by_user_id');
            $table->string('stage');
            $table->string('sub_stage');
            $table->integer('order');
            $table->timestamps();
            // $table->primary(['candidate_id', 'project_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_project');
    }
}
