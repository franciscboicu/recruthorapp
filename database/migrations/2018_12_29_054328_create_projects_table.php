<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_id');
            $table->integer('organization_id');
            $table->integer('partner_id')->default(NULL)->nullable();
            $table->string('title');
            $table->string('department')->default(NULL)->nullable();
            $table->string('country_name')->default(NULL)->nullable();
            $table->string('city_name')->default(NULL)->nullable();
            $table->integer('country_id')->default(NULL)->nullable();
            $table->integer('city_id')->default(NULL)->nullable();
            $table->string('main_skills')->default(NULL)->nullable();
            $table->string('secondary_skills')->default(NULL)->nullable();
            $table->text('job_description');
            $table->text('job_requirements');
            $table->string('employment_type');
            $table->string('industry');
            $table->integer('hours_week')->default(NULL)->nullable();
            $table->boolean('weekends')->default(NULL)->nullable();
            $table->string('education_level');
            $table->string('experience_level');
            $table->integer('total_openings')->nullable()->default(NULL);
            $table->integer('remaining_openings')->nullable()->default(NULL);
            $table->integer('min_salary')->nullable()->default(NULL);
            $table->integer('max_salary')->nullable()->default(NULL);
            $table->string('salary_currency')->nullable()->default('EUR');

            $table->string('f_meal_tickets')->default('off')->nullable();
            $table->string('f_coffee')->default('off')->nullable();
            $table->string('f_relaxation')->default('off')->nullable();
            $table->string('f_gym')->default('off')->nullable();
            $table->string('f_teambuild')->default('off')->nullable();
            $table->string('f_flexible')->default('off')->nullable();
            $table->string('f_insurance')->default('off')->nullable();
            $table->string('f_courses')->default('off')->nullable();
            $table->string('f_relocation')->default('off')->nullable();
            $table->string('f_parking')->default('off')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
