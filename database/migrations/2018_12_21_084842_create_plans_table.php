<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->float('monthly_price');
            $table->float('one_year_price')->default(NULL);
            $table->string('discount_text')->nullable()->default(NULL);
            $table->string('currency')->default('USD');
            $table->string('title', 255)->default("Title")->nullable();
            $table->string('description', 255)->default("Description")->nullable();

            $table->integer('contacts_limit')->default(10);
            $table->integer('users_limit')->default(0);
            $table->integer('projects_limit')->default(2);
            $table->integer('vendors_limit')->default(2);

            $table->boolean('has_portal')->default(0);
            $table->boolean('has_job_landing_pages')->default(0);
            $table->boolean('has_online_applications')->default(0);
            $table->boolean('has_job_tests')->default(0);
            $table->boolean('has_emails')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
