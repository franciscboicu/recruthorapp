<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['person', 'company']);
            $table->integer('owner_id');
            $table->string('person_name')->default(NULL)->nullable();
            $table->text('person_description')->default(NULL)->nullable();
            $table->string('person_country')->default(NULL)->nullable();
            $table->string('company_name')->default(NULL)->nullable();
            $table->string('company_description')->default(NULL)->nullable();
            $table->string('company_reg_number')->default(NULL)->nullable();
            $table->string('company_country')->default(NULL)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
