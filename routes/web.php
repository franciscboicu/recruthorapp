<?php


Route::get('/', 'LandingController@index')->name('landing/a');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');

Route::get('/setup', 'SetupController@index')->name('setup/index');


//routes such as it-recruiting.recruthor.com where it-recruiting is an account organization.
//needed routes:
// {org}.recruthor.com/ - listing "Recruiting for : {vendors}"
// {org}.recruthor.com/vendor/jobs - listing "Jobs for {vendors}"
// {org}.recruthor.com/jobs - listing all jobs mixed
// {org}.recruthor.com/job/slug-slug/id - listing a job, together with the vendor and link to vendor jobs

Route::group(['domain' => 'jobs.recruthor.dev'], function () {
    Route::get('/list', function () {
        print "jobs Listing ";
    });
    Route::get('/show/{jobSlug}/{jobId}', function ($jobSlug, $jobId) {
      print "jobs Listing for " . $jobSlug . " - " . $jobId;
    });
});

Route::group(['domain' => 'projects.recruthor.dev'], function () {
  Route::get('/list', function () {
      print "jobs Listing ";
  });
  Route::get('/show/{jobSlug}/{jobId}', function ($jobSlug, $jobId) {
    print "jobs Listing for " . $jobSlug . " - " . $jobId;
  });
});
   
Route::group(['domain' => 'candidates.recruthor.dev'], function () {

  Route::get('/connect', 'AccountController@organization');

  Route::get('/applications', function () {
    print "candidate applications ";
  });

  Route::get('/jobs/browse', function () {
    print "candidate applications ";
  });

  Route::get('/cv/create', function () {
    print "cv create ";
  });

  Route::get('/cv/edit/{id}', function ($id) {
    print "candidate edit cv " . $id;
  });

});

// Route::group(['domain' => 'partners.recruthor.dev/'], function ($organization) {
//     Route::get('/jobs', function ($organization) {
//         print "jobs for " . $organization;
//     });
// });

Route::get('auth/callback/{provider}', 'SocialAuthController@callback');
Route::get('auth/redirect/{provider}', 'SocialAuthController@redirect');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('{provider}/callback', 'Auth\LoginController@handleProviderCallback');


Route::prefix('candidates')->group(function(){

  Route::get('browse', 'CandidateController@browse')->name('candidates/browse');
  Route::get('read/{id}', 'CandidateController@read')->name('candidates/read');
  Route::get('edit/{id}/job/proposals', 'CandidateController@edit')->name('candidates/edit/job/proposals');
  Route::get('edit/{id}/{sub?}', 'CandidateController@edit')->name('candidates/edit');
  Route::post('edit/{id}', 'CandidateController@edit')->name('candidates/edit');
  Route::get('add', 'CandidateController@add')->name('candidates/add');
  Route::post('add', 'CandidateController@add')->name('candidates/add');
  Route::get('delete/{id}', 'CandidateController@delete')->name('candidates/delete');
  // Route::get('file-upload', 'CandidateController@fileUpload');
  Route::post('upload', 'CandidateController@fileUploadPost')->name('candidates/fileUploadPost');


});

Route::prefix('projects')->group(function(){

  Route::get('browse', 'ProjectController@browse')->name('projects/browse');
  Route::get('read/{id}', 'ProjectController@read')->name('projects/read');
  Route::get('edit/{id}', 'ProjectController@edit')->name('projects/edit');
  Route::post('edit/{id}', 'ProjectController@edit')->name('projects/edit');
  Route::get('add', 'ProjectController@add')->name('projects/add');
  Route::post('add', 'ProjectController@add')->name('projects/add');
  Route::get('delete/{id}', 'ProjectController@delete')->name('projects/delete');
  Route::post('pipeline/state/', 'Projectcontroller@changeCandidatePipelineState')->name('projects/candidate/pipeline/state');
  Route::get('pipeline/candidate/info/{project_id}/{candidate_id}', 'ProjectController@candidateInfo')->name('projects/candidate/info');
  Route::post('/candidate/note/save', 'Projectcontroller@addNote')->name('projects/candidate/note/save');
  Route::post('/candidate/note/delete', 'Projectcontroller@deleteNote')->name('projects/candidate/note/delete');
  Route::post('/pipeline/status/update', 'Projectcontroller@updateStage')->name('pipeline/status/update');

  Route::get('addtoproject/{id}', 'ProjectController@getCandidatesForProject')->name('projects/getCandidatesForProject');

});

Route::prefix('partners')->group(function(){

  Route::get('browse', 'PartnerController@browse')->name('partners/browse');
  Route::get('add', 'PartnerController@add')->name('partners/add');
  Route::post('add', 'PartnerController@add')->name('partners/add');
  Route::get('edit/{id}', 'PartnerController@edit')->name('partner/edit');
  Route::post('edit/{id}', 'PartnerController@edit')->name('partner/edit');

});


Route::prefix('account')->group(function(){

  Route::get('organization', 'AccountController@organization')->name('account/organization');
  Route::get('vendors', 'VendorController@index')->name('account/vendor/list');

});

Route::prefix('files')->group(function(){

  Route::post('remove', 'FileController@remove')->name('files/remove');

});
