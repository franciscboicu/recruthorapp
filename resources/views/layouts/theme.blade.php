<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <title>{{ env('APP_NAME') }}</title>
  <link rel="shortcut icon" href="img/favicon.png">

  @include('header-css')

</head>
<body class="">

@include('preloader')

@include('topbar')

<div class="page-wrap">

  @include('sidebar')

  <div class="page-content">

  @yield('content')

  </div>

</div>

@include('scripts')

@include('overbar')

@yield('script')
</body>
</html>
