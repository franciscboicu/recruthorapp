<script src="{{ asset('theme/vendor/echarts/echarts.min.js')}}"></script>

<script src="{{ asset('theme/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('theme/vendor/popper/popper.min.js')}}"></script>
<script src="{{ asset('theme/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('theme/vendor/select2/js/select2.full.min.js')}}"></script>
<script src="{{ asset('theme/vendor/simplebar/simplebar.js')}}"></script>
<script src="{{ asset('theme/vendor/text-avatar/jquery.textavatar.js')}}"></script>
<script src="{{ asset('theme/vendor/tippyjs/tippy.all.min.js')}}"></script>
<script src="{{ asset('theme/vendor/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{ asset('theme/vendor/wnumb/wNumb.js')}}"></script>
<script src="{{ asset('theme/vendor/sumo-select/jquery.sumoselect.min.js') }}"></script>
<script src="{{ asset('theme/js/main.js')}}"></script>
<script src="{{ asset('theme/vendor/fileapi/FileAPI.html5.min.js') }}"></script>
<script src="{{ asset('theme/js/preview/file-upload.js') }}"></script>

<script src="{{ asset('theme/vendor/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('theme/js/preview/default-dashboard.min.js') }}"></script>


<script src="{{ asset('theme/vendor/tagify/tagify.min.js') }}"></script>
<!-- <script src="{{ asset('theme/js/preview/tag-editor.min.js') }}"></script> -->


<!-- <script src="js/preview/select.min.js"></script> -->

<script src="{{ asset('theme/vendor/sortable/sortable.min.js') }}"></script>
<script src="{{ asset('theme/js/preview/tasks.min.js') }}"></script>
<script src="https://malsup.github.io/jquery.form.js"></script>

<script src="{{ asset('theme/vendor/prism/prism.js') }}"></script>
<script src="{{ asset('theme/vendor/requirejs/require.js') }}"></script>


<script src="{{ asset('theme/vendor/jquery.growl/javascripts/jquery.growl.js') }}"></script>
