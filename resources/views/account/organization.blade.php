@extends('layouts.theme')

@section('content')


<div class="container-fluid">
  <!-- <div class="page-content__header">
    <div>
      <h2 class="page-content__header-heading">Form Inputs</h2>
    </div>
  </div> -->

  <div class="main-container">
    <div class="row">
      <div class="col-md-8">

       
          <h2 class="h2" style="margin-bottom: 0px;">Create a new Project</h2>

      </div>
      <div class="col-md-4">
        @if(1)
        <div class="btn-group btn-collection mr-3 float-right">
          <a href="" class="btn btn-success active">General</a>
          <a href="" class="btn btn-secondary">Pipeline</a>
          <a href="" class="btn btn-secondary ">Settings</a>
          <a href="" class="btn btn-secondary ">Financial</a>
        </div>
        @endif
      </div>
    </div>

  </div>

 
    <form action="{{ route('projects/add') }}" method="POST">

    <div class="row">
      <div class="col-md-8">
        <div class="main-container">

          <div class="row">
            <div class="col-lg-6">
              <h4>Project title</h4>
              <div class="form-group input-group-lg">
                <input type="text" placeholder="Ex: Senior PHP Developer" name="title" class="form-control" value="">
              </div>
            </div>
            <div class="col-lg-6">
              <label for="read-only">Department</label>
              <select  placeholder="Contact Source" name="department" class="form-control">
                    <option @ value=""> Test</option>
              </select>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <h4>Country</h4>
              <div class="form-group input-group-lg">
                <input type="text" placeholder="Ex: Senior PHP Developer" name="country_name" class="form-control" value="">
              </div>
            </div>
            <div class="col-lg-6">
              <h4>City</h4>
              <div class="form-group input-group-lg">
                <input type="text" placeholder="Ex: Senior PHP Developer" name="city_name" class="form-control" value="">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <label for="read-only">Type</label>
              <select  placeholder="Contact Source" name="employment_type" class="form-control">
                    <option value=""> 123 </option>
              </select>
            </div>
            <div class="col-lg-6">
              <label for="read-only">Industry</label>
              <select  placeholder="Contact Source" name="employment_type" class="form-control">
                <option value=""> 123 </option>
             </select>
            </div>
          </div>

          <br/>

          <div class="row">
            <div class="col-lg-6">
              <label for="read-only">Education Level</label>
              <select  placeholder="Contact Source" name="employment_type" class="form-control">
                <option value=""> 123 </option>
              </select>
            </div>
            <div class="col-lg-6">
              <label for="read-only">Experience Needed</label>
              <select  placeholder="Contact Source" name="employment_type" class="form-control">
                <option value=""> 123 </option>
              </select>
            </div>
          </div>

          <br/>

        
        </div>
      </div>
      <div class="col-md-4">

        include('projects.projects-add-rightsidebar')


      </div>
    </div>

  @csrf

  <div class="main-container">
    <div class="row">
      <div class="col-lg-10">
        <p><small>This section helps you create a new project. Projects are the second important things in this software, because Candidates revolve around them.</small></p>
      </div>
      <div class="col-lg-2">
          <button  class="float-right btn btn-success"> Add this candidate </button>

    </div>
    </div>
  </div>

  <!-- <div id="sub">Save</div> -->
</form>

</div>


@endsection

@section('script')
  <script>

  (function ($) {
    'use strict';

    $(document).ready(function() {
      var mainSkills = new Tagify($('#main-skills').get(0));
      var secondarySkills = new Tagify($('#secondary-skills').get(0));
      //
      // $('#selectbox-ex1').SumoSelect();
      // $('#selectbox-ex2').SumoSelect();
      // $('#selectbox-ex3').SumoSelect();
      // $('#selectbox-ex4').SumoSelect();
    });
  })(jQuery);




  </script>
@endsection
