@extends('layouts.theme')

@section('content')

<div class="container-fluid container-fh" style="margin-top:30px;">
  <div class="main-container container-fh__content m-social-profile">
    <div class="m-social-profile__cover" style="background-image: url('/theme/img/social-profile/8.png'); height:40px;">
      <div class="m-social-profile__profile-image" style="margin-bottom:-30px;">
        <img src="https://via.placeholder.com/160" alt="" class=" rounded-circle">
      </div>
    </div>
    <div class="m-social-profile__content">
      <div class="m-social-profile__info">
        <div class="m-social-profile__info-user">
          <div class="m-social-profile__info-username">{{ $candidate->fullname }}</div>
          <!-- <div class="m-social-profile__info-user-actions">
            <button class="btn btn-success m-social-profile__info-user-action">Follow</button>
            <button class="btn btn-info m-social-profile__info-user-action">Message</button>
          </div> -->
        </div>

      <div class="card-widget card-widget-b">
        <div class="card-widget-b__stats">
          <div class="card-widget-b__stats-item">
            <span class="ua-icon-phone card-widget-b__stats-item-icon"></span>
            <div class="card-widget-b__stats-item-content">
              <div class="card-widget-b__stats-item-value">{{$candidate->phone}}</div>
              <div class="card-widget-b__stats-item-desc">Mobile</div>
            </div>
          </div>
          <!-- <div class="card-widget-b__stats-item">
            <span class="ua-icon-phone card-widget-b__stats-item-icon"></span>
            <div class="card-widget-b__stats-item-content">
              <div class="card-widget-b__stats-item-value">+1 (800) 234-5678</div>
              <div class="card-widget-b__stats-item-desc">Work</div>
            </div>
          </div> -->
          <div class="card-widget-b__stats-item">
            <span class="ua-icon-envelope card-widget-b__stats-item-icon"></span>
            <div class="card-widget-b__stats-item-content">
              <div class="card-widget-b__stats-item-value">{{$candidate->email}}</div>
              <div class="card-widget-b__stats-item-desc">Personal</div>
            </div>
          </div>

          <div class="card-widget-b__stats-item">
            <span class="ua-icon-linkedin card-widget-b__stats-item-icon"></span>
            <div class="card-widget-b__stats-item-content">
              <div class="card-widget-b__stats-item-value">{{$candidate->linkedin_profile}}</div>
              <div class="card-widget-b__stats-item-desc">LINKEDIN</div>
            </div>
          </div>

          <div class="card-widget-b__stats-item">
            <span class="ua-icon-user card-widget-b__stats-item-icon"></span>
            <div class="card-widget-b__stats-item-content">
              <div class="card-widget-b__stats-item-value">{{$candidate->skype_id}}</div>
              <div class="card-widget-b__stats-item-desc">SKYPE</div>
            </div>
          </div>

          <div class="card-widget-b__stats-item">
            <span class="ua-icon-map-pin card-widget-b__stats-item-icon"></span>
            <div class="card-widget-b__stats-item-content">
              <div class="card-widget-b__stats-item-value">
                <div>1600 Amphiteatre Pkwy</div>
                <div>Mountain View, CA, 94043</div>
              </div>
              <div class="card-widget-b__stats-item-desc">Work</div>
            </div>
          </div>
        </div>
      </div>



      </div>
      <div class="m-social-profile__tabs">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item m-social-profile__tab-item">
            <a class="nav-link m-social-profile__tab-link active" id="home-tab" data-toggle="tab" href="#social-profile-activities" role="tab" aria-controls="home" aria-selected="true">Activities</a>
          </li>
          <li class="nav-item m-social-profile__tab-item">
            <a class="nav-link m-social-profile__tab-link" id="profile-tab" data-toggle="tab" href="#social-profile-followers" role="tab" aria-controls="profile" aria-selected="false">Projects</a>
          </li>
          <!-- <li class="nav-item m-social-profile__tab-item">
            <a class="nav-link m-social-profile__tab-link" id="contact-tab" data-toggle="tab" href="#social-profile-following" role="tab" aria-controls="contact" aria-selected="false">Following</a>
          </li> -->
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade show active" id="social-profile-activities">
            <div class="m-social-profile__activities">
              @if(count($activities) > 0)
                @foreach($activities as $activity)
                <div class="m-social-profile__activity-item">

                  <div class="m-social-profile__activity-item-header">
                    <img src="https://via.placeholder.com/56" alt="" class="m-social-profile__activity-item-avatar rounded-circle" width="56" height="56">
                    <div class="m-social-profile__activity-item-info">
                      <div>
                        {!! $activity->getActivityText() !!}

                        {{$activity->user->email}}
                      </div>
                      <div>
                        <span class="m-social-profile__activity-item-date">
                          <span class="ua-icon-datepicker m-social-profile__activity-item-icon"></span> <span>{{ $activity->getActivityTime() }}</span>
                        </span>
                        <span class="m-social-profile__activity-item-location">
                          <span class="ua-icon-map m-social-profile__activity-item-icon"></span> <span>Las Vegas</span>
                        </span>
                      </div>
                    </div>

                  </div>
                </div>
                @endforeach
              @else
              <div class="container text-center">
                <p class="text-muted mt-5">
                  There are now activities related to this candidate. <br/>
                  Once this candidate is involved in a recruitment process, activities will keep showing up here.
                </p>
              </div>
              @endif


              <!-- <div class="m-social-profile__load-more-block">
                <a href="#" class="btn btn-outline-secondary icon-left mr-3">
                  Load More Activities <span class="btn-icon ua-icon-update"></span>
                </a>
              </div> -->
            </div>
          </div>

          <div class="tab-pane fade" id="social-profile-followers">
            <div class="m-social-profile__followers">

              @if($candidateProjects)
                @foreach($candidateProjects as $candidateProject)

                <div class="m-social-profile__follower">
                  <img src="/theme/img/users/user-15.png" alt="" class="m-social-profile__follower-avatar rounded-circle" width="56" height="56">
                  <div class="m-social-profile__follower-info">
                    <div>
                      <a href="{{route('projects/read', ['id' => $candidateProject->project_id ])}}" class="link-info">{{$candidateProject->project->title}}</a>
                    </div>
                    <div class="m-social-profile__follower-info-desc">
                      @ {{$candidateProject->project->company->name}}
                    </div>
                  </div>
                  <div class="m-social-profile__follower-actions">
                    @if("" == $candidateProject->sub_stage)
                    <a href="#" class="btn btn-warning">In Workflow</a>
                    @endif

                    @if("rejected" == $candidateProject->sub_stage)
                    <a href="#" class="btn btn-danger">Rejected </a>
                    @endif

                    @if("hired" == $candidateProject->sub_stage)
                    <a href="#" class="btn btn-success">Hired </a>
                    @endif


                  </div>
                </div>

                @endforeach
              @endif


              <!-- <div class="m-social-profile__load-more-block">
                <a href="#" class="btn btn-outline-secondary icon-left">
                  Load More Followers <span class="btn-icon ua-icon-update"></span>
                </a>
              </div> -->
            </div>
          </div>
          <div class="tab-pane fade" id="social-profile-following">
            <div class="m-social-profile__followers">
              <div class="m-social-profile__follower">
                <img src="/theme/img/users/user-15.png" alt="" class="m-social-profile__follower-avatar rounded-circle" width="56" height="56">
                <div class="m-social-profile__follower-info">
                  <div>
                    <a href="#" class="link-info">Antonius</a>
                  </div>
                  <div class="m-social-profile__follower-info-desc">
                    @antonius
                  </div>
                </div>
                <div class="m-social-profile__follower-actions">
                  <a href="#" class="btn btn-success">Following</a>
                </div>
              </div>
              <div class="m-social-profile__follower">
                <img src="/theme/img/users/user-16.png" alt="" class="m-social-profile__follower-avatar rounded-circle" width="56" height="56">
                <div class="m-social-profile__follower-info">
                  <div>
                    <a href="#" class="link-info">Antonius</a>
                  </div>
                  <div class="m-social-profile__follower-info-desc">
                    @antonius
                  </div>
                </div>
                <div class="m-social-profile__follower-actions">
                  <a href="#" class="btn btn-outline-secondary icon-left">
                    Follow <span class="btn-icon ua-icon-plus"></span>
                  </a>
                </div>
              </div>
              <div class="m-social-profile__follower">
                <img src="/theme/img/users/user-17.png" alt="" class="m-social-profile__follower-avatar rounded-circle" width="56" height="56">
                <div class="m-social-profile__follower-info">
                  <div>
                    <a href="#" class="link-info">Antonius</a>
                  </div>
                  <div class="m-social-profile__follower-info-desc">
                    @antonius
                  </div>
                </div>
                <div class="m-social-profile__follower-actions">
                  <a href="#" class="btn btn-outline-secondary icon-left">
                    Follow <span class="btn-icon ua-icon-plus"></span>
                  </a>
                </div>
              </div>
              <div class="m-social-profile__follower">
                <img src="/theme/img/users/user-18.png" alt="" class="m-social-profile__follower-avatar rounded-circle" width="56" height="56">
                <div class="m-social-profile__follower-info">
                  <div>
                    <a href="#" class="link-info">Antonius</a>
                  </div>
                  <div class="m-social-profile__follower-info-desc">
                    @antonius
                  </div>
                </div>
                <div class="m-social-profile__follower-actions">
                  <a href="#" class="btn btn-danger">Unfollow</a>
                </div>
              </div>
              <div class="m-social-profile__load-more-block">
                <a href="#" class="btn btn-outline-secondary icon-left">
                  Load More Followers <span class="btn-icon ua-icon-update"></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection

@section('script')
  <script>

  (function ($) {
    'use strict';

    $(document).ready(function() {
      var mainSkills = new Tagify($('#main-skills').get(0));
      var secondarySkills = new Tagify($('#secondary-skills').get(0));
      //
      // $('#selectbox-ex1').SumoSelect();
      // $('#selectbox-ex2').SumoSelect();
      // $('#selectbox-ex3').SumoSelect();
      // $('#selectbox-ex4').SumoSelect();
    });
  })(jQuery);




  </script>
@endsection
