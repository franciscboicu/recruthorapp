@extends('layouts.theme')

@section('content')


<div class="container-fluid m-invoices scroll">
  <div class="main-container m-invoices__container">
    <div class="m-invoices__tables">
      <div class="table-header">
        @if(0 < $candidatesCount)
        <h4 class="table-header__heading">Candidates</h4>
        <div class="table-header__controls">
            <a href="{{ route('candidates/add') }}" class="btn btn-success "> Add new candidate</a>
        </div>
        @endif
      </div>
      @if(0 < $candidatesCount)
      <table class="table table-hover table__actions">
        <tbody>

          @foreach($candidates as $i=>$candidate)

          <tr>
            <td class="table__avatar" width="250px">
              <img src="/theme/img/users/user-{{$i+2}}.png" alt="" width="34" height="34" class="rounded-circle">
              <a href="{{ route('candidates/read',['id' => $candidate->id]) }}" class="widget-notifications__user"><span>{{ $candidate->fullname }}</span></a>
            </td>
            <td>
              @foreach($candidate->getMainSkillsList() as $mainSkill)
                <span class="badge badge-pill badge-success badge-small"><small>{{ $mainSkill }}</small></span>
              @endforeach
              @foreach($candidate->getSecondarySkillsList() as $secondarySkill)
                <span class="badge badge-pill badge-warning badge-small"><small>{{ $secondarySkill }}</small></span>
              @endforeach
            </td>
            <td></td>
            <td></td>
            <td>
              <span class="table__tag table__tag--danger table__tag--lg">

              </span>
            </td>
            <td class="table__cell-actions">
              <div class="table__cell-actions-wrap">
                <a href="#" class="table__cell-actions-item table__cell-actions-icon">
                  <span class="ua-icon-send"></span>
                </a>
                <a href="#" class="table__cell-actions-item table__cell-actions-icon">
                  <span class="ua-icon-remove"></span>
                </a>
                <a href="{{ route('candidates/edit', ['id' => $candidate->id]) }}" class="table__cell-actions-item table__cell-actions-icon">
                  <span class="ua-icon-edit-outline"></span>
                </a>
                <!-- <a href="#" class="table__cell-actions-item table__cell-actions-icon">
                  <span class="ua-icon-print"></span>
                </a> -->
                <a href="#" class="table__cell-actions-item table__cell-actions-icon">
                  <span class="ua-icon-info"></span>
                </a>
              </div>
            </td>
          </tr>

          @endforeach

        </tbody>
      </table>
      @else
        <div class="row" style="margin-top:200px;">
          <div class="col-md-4"></div>
          <div class="col-md-4 text-center">
            <img src="{{asset('theme/img/sad.png')}}" width="128px" height="128px"/>
            <br/>
            <h2>Make this guy happy!</h2>
            and <a href="{{ route('candidates/add') }}" class="btn btn-success ">
              add your first contact
            </a>
          </div>
          <div class="col-md-4"></div>
        </div>
      @endif

      <div class="mt-5 ml-5 mb-5 mr-5 float-right">
          {{ $candidates->links() }}
      </div>

    </div>

  </div>
</div>


@endsection
