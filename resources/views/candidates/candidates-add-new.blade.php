@extends('layouts.theme')

@section('content')

<style>
       .progress { position:relative; width:100%; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; }
       .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }
       .percent { position:absolute; display:inline-block; top:3px; left:48%; color: #7F98B2;}
   </style>

<div class="container-fluid">
  <!-- <div class="page-content__header">
    <div>
      <h2 class="page-content__header-heading">Form Inputs</h2>
    </div>
  </div> -->



  <div class="main-container">
    <div class="row">
      <div class="col-md-8">

        @if(isset($candidate))
        <h2 class="h2" style="margin-bottom: 0px;">[Edit] {{$candidate->fullname}} #{{$candidate->id}}</h2>
        @else
        <h2 class="h2" style="margin-bottom: 0px;">Add a new Candidate New</h2>
        @endif

      </div>
      <div class="col-md-4 col-xs-12">
        <div class="btn-group btn-collection float-right">
          @if(isset($candidate))
          <a href="{{ route('candidates/edit', ['id' => $candidate->id ]) }}" class="btn @if(!isset($_GET['sub'])) btn-success @endif active">Basic info</a>
          <a href="{{ route('candidates/edit', ['id' => $candidate->id, 'sub' => 'details' ]) }}" class="btn @if(isset($_GET['sub']) && $_GET['sub'] == 'details') btn-success @endif  active">Details</a>
          <a href="{{ route('candidates/edit', ['id' => $candidate->id, 'sub' => 'jobs' ]) }}" class="btn @if(isset($_GET['sub']) && $_GET['sub'] == 'jobs') btn-success @endif  active">Projects</a>
          <a href="{{ route('candidates/edit', ['id' => $candidate->id, 'sub' => 'docs' ]) }}" class="btn @if(isset($_GET['sub']) && $_GET['sub'] == 'docs') btn-success @endif  active">Documents</a>

          @else
          <a href="" class="btn btn-success active">Basic info</a>
          <a href="" class="btn btn-secondary disabled">Details</a>
          <a href="" class="btn btn-secondary disabled">Projects</a>
          <a href="" class="btn btn-secondary disabled">Documents</a>

          @endif
        </div>
      </div>
    </div>

  </div>

@if(!isset($_GET['sub']))

  @if(isset($candidate))
  <form action="{{ route('candidates/edit', ['id'=>$candidate->id]) }}" method="POST">
  @else
    <form action="{{ route('candidates/add') }}" method="POST">
  @endif


  <div class="row">
    <div class="col-md-6 ">
      <div class="main-container">
        
          <h4>Full name</h4>
          <div class="form-group input-group-lg">
            <input type="text" placeholder="" name="fullname" class="form-control" value="@if(isset($candidate)){{ $candidate->fullname }}@endif">
          </div>
          <div class="form-group input-group-lg">
            <label for="disabled">Skype</label>
            <input type="text" placeholder="" name="skype_id" class="form-control"  value="@if(isset($candidate)){{ $candidate->skype_id }}@endif">
          </div>

          <h4>Phone Number</h4>
          <div class="form-group input-group-lg">
            <input type="text" placeholder="" name="phone" class="form-control"  value="@if(isset($candidate)){{ $candidate->phone }}@endif">
          </div>
          <div class="form-group input-group-lg">
            <label for="read-only">Linkedin Profile</label>
            <input type="text" placeholder="" name="linkedin_profile" class="form-control" value="@if(isset($candidate)){{ $candidate->linkedin_profile }}@endif">
          </div>

          <h4>Email</h4>
          <div class="form-group input-group-lg">
            <input type="text" placeholder="" name="email" class="form-control" value="@if(isset($candidate)){{ $candidate->email }}@endif">
          </div>

            <div class="form-group input-group-lg">
              <label for="read-only">Source</label>
              <select  placeholder="Contact Source" name="source" class="form-control">
                @foreach($candidateSources as $groupKey => $sourceGroup)
                  <option disabled>{{$groupKey}}</option>
                    @foreach($sourceGroup as $sourceKey=> $sourceLabel)
                      <option @if(isset($candidate) && $candidate->source == $sourceKey) selected @endif value="{{$sourceKey}}"> {{ $sourceLabel }} </option>
                    @endforeach
                @endforeach
              </select>
            </div>

      </div> 
    </div>

    <div class="col-md-6">
      <div class="main-container">
      
          <h4>Country</h4>
          <div class="form-group input-group-lg">
            <input type="text" placeholder="" name="country_name" class="form-control"  value="@if(isset($candidate)) {{ $candidate->country_name }}@endif">
          </div>

          <h4>City</h4>
          <div class="form-group input-group-lg">
            <input type="text" placeholder="" name="city_name" class="form-control"  value="@if(isset($candidate)) {{ $candidate->city_name }}@endif">
          </div>

          <h4>Wishes for Relocation?</h4>
          <div class="form-group input-group-lg">
            <select  placeholder="Contact Source" name="relocation" class="form-control">
              <option value="0" @if(isset($candidate) && $candidate->relocation == 0) selected @endif> No </option>
              <option value="1" @if(isset($candidate) && $candidate->relocation == 1) selected @endif> Yes </option>
            </select>
          </div>

          <h4>Wants to work remotely?</h4>
          <div class="form-group input-group-lg">
            <select  placeholder="Contact Source" name="remote" class="form-control">
              <option value="0" @if(isset($candidate) && $candidate->remote == 0) selected @endif> No </option>
              <option value="1" @if(isset($candidate) && $candidate->remote == 1) selected @endif> Yes </option>
            </select>
          </div>

      </div> 
    </div>
  </div>

    @csrf

    <div class="main-container container-fh__content">
      <div class="row">
        <div class="col-lg-6">

          <label for="main-skills">Main Skills</label>

          <div class="tag-editor"> <!-- need wrap -->
            <textarea id="main-skills" name="main_skills" placeholder='Write some tags'>@if(isset($candidate)) {{ $candidate->main_skills }}@endif</textarea>
          </div>
        </div>
        <div class="col-lg-6">

          <label for="secondary-skills">Secondary Skills</label>

          <div class="tag-editor"> <!-- need wrap -->
            <textarea id="secondary-skills" name="secondary_skills" placeholder='Write some tags'>@if(isset($candidate)) {{ $candidate->secondary_skills }}@endif</textarea>
          </div>
        </div>
      </div>
    </div>

    <div class="main-container">
      <div class="row">
        <div class="col-lg-10">
          <p><small>This section helps you add a new candidate. Candidates are at the core of Recruthor so almost every feature revolves around them.</small></p>
        </div>
        <div class="col-lg-2">
          <button  class="float-right btn btn-success"> @if(isset($candidate)) Update @else Add @endif this candidate </button>
      </div>
      </div>
    </div>

    <!-- <div id="sub">Save</div> -->
  </form>

@endif

@if(isset($_GET['sub']) && $_GET['sub'] == 'docs')

<div class="main-container">
  <form method="POST" action="{{ route('candidates/fileUploadPost') }}" id="uploadForm" enctype="multipart/form-data">
  <div class="row">
      <div class="col-lg-3">
          @csrf
          <div class="form-group">
              <input name="file" type="file" class="form-control" id="file" accept="application/pdf" >
              <input name="candidate_id" type="hidden" class="form-control" id="candidate_id" value="{{$candidate->id}}">

          </div>
      </div>
      <div class="col-lg-7">
        <input type="text" class="form-control" maxlength="64" name="note" id="note" placeholder="Insert a note related to the file, Example: last CV Version" />
      </div>
      <div class="col-lg-2">
        <input type="submit"  value="Upload" class="btn btn-success btn-block" id="uploadBtn">

      </div>
  </div>
</form>
</div>

<div class="main-container">
  @foreach($candidateDocs as $candidateDoc)
    <div class="row mb-2 fileRow" >

        <div class="col-md-10">
          {{$candidateDoc->note}}
        </div>

        <div class="col-md-2">
          <a href="/files/{{$candidateDoc->filename}}" class="btn btn-success" target="_blank"> Open </a>
          <button class="btn btn-warning removeFile" data-filename="{{$candidateDoc->filename}}"> Delete </button>
        </div>

    </div>
    <hr/>
  @endforeach
</div>


@endif

</div>


@endsection

@section('script')
  <script>

  (function ($) {
    'use strict';

    $(document).ready(function() {
      var mainSkills = new Tagify($('#main-skills').get(0));
      var secondarySkills = new Tagify($('#secondary-skills').get(0));
      //
      // $('#selectbox-ex1').SumoSelect();
      // $('#selectbox-ex2').SumoSelect();
      // $('#selectbox-ex3').SumoSelect();
      // $('#selectbox-ex4').SumoSelect();
    });
  })(jQuery);


  function validate(formData, jqForm, options) {
       var form = jqForm[0];
       if (!form.file.value) {
           alert('Click Choose file and select a PDF File!');
           return false;
       }

       if (!form.note.value) {
           alert('Please insert a note');
           return false;
       }
   }

   $(document).ready(function () {


     $(".removeFile").click(function(event){
       event.preventDefault();
       fileName = $(this).data("filename");

       $.ajax({
           type: "POST",
           url: "{{route('files/remove')}}",
           data: {
            "_token": '{{csrf_token()}}',
            "filename": fileName
           },
           dataType: false,
           success: function (data) {
              $.growl.notice({ title: "File removed!", message: data.message, size: 'large', location: 'tc'  });
               setInterval(function(){
                 window.location.reload();
               }, 1500);
           },
           error: function (e) {
             console.log(e);
               $.growl.error({ title: "File not removed", message: e.responseJSON.message, size: 'large', location: 'tc' });
           }
       });

     });

     $("#uploadBtn").click(function (event) {
         event.preventDefault();
         var form = $('#uploadForm')[0];
         var data = new FormData(form);
         $("#uploadBtn").prop("disabled", true);


         // if (!form.file.value) {
         //
         // }

         $.ajax({
             type: "POST",
             enctype: 'multipart/form-data',
             url: "{{route('candidates/fileUploadPost')}}",
             data: data,
             processData: false,
             contentType: false,
             cache: false,
             timeout: 600000,
             success: function (data) {
                $.growl.notice({ title: "Upload", message: "Your file was uploaded!", size: 'large', location: 'tc'  });
                 $("#uploadBtn").prop("disabled", false);
                 form.reset();
                 setInterval(function(){
                   window.location.reload();
                 }, 1500);
             },
             error: function (e) {
               console.log(e);
                 $.growl.error({ title: "Upload", message: e.responseJSON.message, size: 'large', location: 'tc' });
                 $("#uploadBtn").prop("disabled", false);
                 form.reset();

             }
         });

       });

   });

  </script>
@endsection
