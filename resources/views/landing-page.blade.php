<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Recruthor</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

  <!-- Facebook and Twitter integration -->
	<!-- <meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" /> -->

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400" rel="stylesheet">

	<!-- Animate.css -->
	<link rel="stylesheet" href="/landing-page/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="/landing-page/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="/landing-page/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="/landing-page/css/magnific-popup.css">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="/landing-page/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/landing-page/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="/landing-page/css/style.css">

	<!-- Modernizr JS -->
	<script src="/landing-page/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="/landing-page/js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>

	<div class="colorlib-loader"></div>

	<div id="page">
		<nav class="colorlib-nav" role="navigation">
			<div class="top-menu">
				<div class="container">
					<div class="row">
						<div class="col-md-2">
							<div id="colorlib-logo"><a href="index.html">Recru<font color="#000">Thor</font></a></div>
						</div>
						<div class="col-md-10 text-right menu-1">
							<ul>
								<li class="active"><a href="index.html">Home</a></li>
								<li class="">
									<a href="#features">Features</a>
									<!-- <ul class="dropdown">
										<li><a href="work-grid.html">Works grid with text</a></li>
										<li><a href="work-grid-without-text.html">Works grid w/o text</a></li>
									</ul> -->
								</li>
								<li><a href="#video">Video</a></li>
								<li><a href="#pricing">Pricing</a></li>
								<li><a href="#faq">F.A.Q.</a></li>
								<li><a href="#contact">Contact</a></li>
								<li><a class=""  href="{{route('login')}}">Account</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</nav>

<!-- https://www.youtube.com/watch?v=vqqt5p0q-eU -->
		<section id="home" class="video-hero" style="height: 700px; background-image: url(landing-page/images/cover_img_1.jpg);  background-size:cover; background-position: center center;background-attachment:fixed;" data-section="home">
		<div class="overlay"></div>
			<a class="player" data-property="{videoURL:'',containment:'#home', showControls:false, autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, quality:'default'}"></a>
			<div class="display-t text-center">
				<div class="display-tc">
					<div class="container">
						<div class="col-md-12 col-md-offset-0">
							<div class="animate-box">
								<h2>Bla bla</h2>
								<p>using these amazing features offered by Recruthor</p>

								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-6">
									<form class="form-inline qbstp-header-subscribe">
										<div class="col-three-forth">
											<div class="form-group">
												<input type="text" class="form-control" id="email" placeholder="Enter your email">
											</div>
										</div>
										<div class="col-one-third">
											<div class="form-group">
												<button type="submit" class="btn btn-primary">Start for free</button>
											</div>
										</div>
									</form>
									</div>
									<div class="col-md-3"></div>
								</div>
								<!-- <p><a href="#" class="btn btn-primary btn-lg btn-custom">Start for Free</a></p>
-->
								<small style="color: #fff;"> - or - </small>
								<p> find <a href="#section-features" style="color: black;"><strong>out more</strong></a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="colorlib-featured">
			<div class="row animate-box">
				<div class="featured-wrap">
					<div class="owl-carousel">
						<div class="item">
							<div class="col-md-8 col-md-offset-2">
								<div class="featured-entry">
									<img class="img-responsive" src="/landing-page/images/dashboard_full_4.png" alt="">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="col-md-8 col-md-offset-2">
								<div class="featured-entry">
									<img class="img-responsive" src="/landing-page/images/dashboard_full_5.png" alt="">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="col-md-8 col-md-offset-2">
								<div class="featured-entry">
									<img class="img-responsive" src="/landing-page/images/dashboard_full_1.jpg" alt="">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="col-md-8 col-md-offset-2">
								<div class="featured-entry">
									<img class="img-responsive" src="/landing-page/images/dashboard_full_2.jpg" alt="">
								</div>
							</div>
						</div>
						<div class="item">
							<div class="col-md-8 col-md-offset-2">
								<div class="featured-entry">
									<img class="img-responsive" src="/landing-page/images/dashboard_full_3.jpg" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="colorlib-services colorlib-bg-white" id="section-features">
			<div class="container">
				<div class="row">
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="icon-browser"></i>
							</span>
							<div class="desc">
								<h3>Create brands for Employers</h3>
								<p>Using RecruThor you can create branding pages for the companies you recruit for and the projects, that you can use in your campaigns.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="icon-download"></i>
							</span>
							<div class="desc">
								<h3>Track the progress</h3>
								<p>Recruthor helps you track the progress of each candidate, very easy and with an amazing interface. Every action is made in the shortest way possible to help you save time.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="icon-layers"></i>
							</span>
							<div class="desc">
								<h3>Always connected</h3>
								<p>Stay connected with your candidates, by notifying them when you change their status in the pipeline. </p>
							</div>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="icon-browser"></i>
							</span>
							<div class="desc">
								<h3>Create brands for Employers</h3>
								<p>Using RecruThor you can create branding pages for the companies you recruit for and the projects, that you can use in your campaigns.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="icon-download"></i>
							</span>
							<div class="desc">
								<h3>Automatic Backup Data</h3>
								<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center animate-box">
						<div class="services">
							<span class="icon">
								<i class="icon-layers"></i>
							</span>
							<div class="desc">
								<h3>Page Builder</h3>
								<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>

		<div class="colorlib-intro">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
						<h2>Collaborate with your design team in a new way</h2>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 animate-box">
						<span class="play"><a href="https://vimeo.com/channels/staffpicks/93951774" class="pulse popup-vimeo"><i class="icon-play3"></i></a></span>
					</div>
				</div>
			</div>
		</div>

		<div class="colorlib-work-featured colorlib-bg-white">
			<div class="container">
				<div class="row mobile-wrap">
					<div class="col-md-5 animate-box">
						<div class="mobile-img" style="background-image: url(landing-page/images/mobile-2.jpg);"></div>
					</div>
					<div class="col-md-7 animate-box">
						<div class="desc">
							<h2>Real template creation</h2>
							<div class="features">
								<span class="icon"><i class="icon-lightbulb"></i></span>
								<div class="f-desc">
									<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="icon-circle-compass"></i></span>
								<div class="f-desc">
									<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="icon-beaker"></i></span>
								<div class="f-desc">
									<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
								</div>
							</div>
							<p><a href="#" class="btn btn-primary btn-outline with-arrow">Start collaborating <i class="icon-arrow-right3"></i></a></p>
						</div>
					</div>
				</div>
				<div class="row mobile-wrap">
					<div class="col-md-5 col-md-push-7 animate-box">
						<div class="mobile-img" style="background-image: url(landing-page/images/mobile-1.jpg);"></div>
					</div>
					<div class="col-md-7 col-md-pull-5 animate-box">
						<div class="desc">
							<h2>Finish template creation</h2>
							<div class="features">
								<span class="icon"><i class="icon-lightbulb"></i></span>
								<div class="f-desc">
									<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="icon-circle-compass"></i></span>
								<div class="f-desc">
									<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p>
								</div>
							</div>
							<div class="features">
								<span class="icon"><i class="icon-beaker"></i></span>
								<div class="f-desc">
									<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
								</div>
							</div>
							<p><a href="#" class="btn btn-primary btn-outline with-arrow">Start collaborating <i class="icon-arrow-right3"></i></a></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(landing-page/images/cover_img_1.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4 col-sm-4 text-center animate-box">
							<div class="counter-entry">
								<span class="icon"><i class="flaticon-ribbon"></i></span>
								<div class="desc">
									<span class="colorlib-counter js-counter" data-from="0" data-to="1500" data-speed="5000" data-refresh-interval="50"></span>
									<span class="colorlib-counter-label">Of customers are satisfied with our professional support</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 text-center animate-box">
							<div class="counter-entry">
								<span class="icon"><i class="flaticon-church"></i></span>
								<div class="desc">
									<span class="colorlib-counter js-counter" data-from="0" data-to="500" data-speed="5000" data-refresh-interval="50"></span>
									<span class="colorlib-counter-label">Amazing preset options to be mixed and combined</span>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 text-center animate-box">
							<div class="counter-entry">
								<span class="icon"><i class="flaticon-dove"></i></span>
								<div class="desc">
									<span class="colorlib-counter js-counter" data-from="0" data-to="1200" data-speed="5000" data-refresh-interval="50"></span>
									<span class="colorlib-counter-label">Average response time on live chat support channel</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="colorlib-blog">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
						<h2>News from our Blog</h2>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 animate-box">
						<article>
							<h2>Building the Mention Sales Application on Unapp</h2>
							<p class="admin"><span>May 12, 2018</span></p>
							<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
							<p class="author-wrap"><a href="#" class="author-img" style="background-image: url(landing-page/images/person1.jpg);"></a> <a href="#" class="author">by Dave Miller</a></p>
						</article>
					</div>
					<div class="col-md-4 animate-box">
						<article>
							<h2>Building the Mention Sales Application on Unapp</h2>
							<p class="admin"><span>May 12, 2018</span></p>
							<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
							<p class="author-wrap"><a href="#" class="author-img" style="background-image: url(landing-page/images/person2.jpg);"></a> <a href="#" class="author">by Dave Miller</a></p>
						</article>
					</div>
					<div class="col-md-4 animate-box">
						<article>
							<h2>Building the Mention Sales Application on Unapp</h2>
							<p class="admin"><span>May 12, 2018</span></p>
							<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
							<p class="author-wrap"><a href="#" class="author-img" style="background-image: url(landing-page/images/person3.jpg);"></a> <a href="#" class="author">by Dave Miller</a></p>
						</article>
					</div>
				</div>
			</div>
		</div>

		<div id="colorlib-subscribe" class="colorlib-subscribe" style="background-image: url(landing-page/images/cover_img_1.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center colorlib-heading animate-box">
						<h2>Already trusted by over 10,000 users</h2>
						<p>Subscribe to receive unapp tips from instructors right to your inbox.</p>
					</div>
				</div>
				<div class="row animate-box">
					<div class="col-md-6 col-md-offset-3">
						<div class="row">
							<div class="col-md-12">
							<form class="form-inline qbstp-header-subscribe">
								<div class="col-three-forth">
									<div class="form-group">
										<input type="text" class="form-control" id="email" placeholder="Enter your email">
									</div>
								</div>
								<div class="col-one-third">
									<div class="form-group">
										<button type="submit" class="btn btn-primary">Subscribe Now</button>
									</div>
								</div>
							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php use App\Plan; ?>

		@if(count($plans) > 0)

		<div class="colorlib-pricing" id="pricing" style="margin-top:-100px;">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
						<h2>Pricing</h2>
						<p>Stop using Excel and accelerate your recruiting processes</p>
					</div>
				</div>
				<div class="row">

						@foreach($plans as $plan)

						<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">

							<div class="panel price {{ Plan::PANELS[$plan->slug] }}">
								<div class="panel-heading arrow_box text-center" >
								<h3 style="color:#fff; margin-top:5px;">{{strtoupper($plan->name)}}</h3>
								</div>
								<div class="panel-footer">
									<a class="btn btn-lg btn-block {{ Plan::BUTTONS[$plan->slug] }}" href="#">{{ $plan->title }}</a>
								</div>
								<ul class="list-group list-group-flush text-center">
									<li class="list-group-item"><i class="icon-ok text-success"></i> Personal use</li>
									<li class="list-group-item"><i class="icon-ok text-success"></i> Unlimited projects</li>
									<li class="list-group-item"><i class="icon-ok text-success"></i> 27/7 support</li>
								</ul>

								<div class="panel-body text-center">
									<p class="lead" style="font-size:33px"><strong>${{$plan->monthly_price}} / month</strong></p>
								</div>

							</div>
							<!-- /PRICE ITEM -->

						</div>

						@endforeach

						<div class="col-md-3 text-center animate-box">
							<div class="pricing">
								<h2 class="pricing-heading">Very Custom</h2>
								<div class="price"><sup class="currency">$</sup>?<small>per month</small></div>
								<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
								<p><a href="#" class="btn btn-primary">Contact Us</a></p>
							</div>
						</div>

				</div>
			</div>
		</div>

		@endif

		<footer id="colorlib-footer">
			<div class="container">
				<div class="row row-pb-md">
					<div class="col-md-3 colorlib-widget">
						<h4>About Recruthor</h4>
						<p> Separated they live in Bookmarksgrove right at the coast of the Semantics</p>
						<p>
							<ul class="colorlib-social-icons">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-linkedin"></i></a></li>
								<li><a href="#"><i class="icon-dribbble"></i></a></li>
							</ul>
						</p>
					</div>
					<div class="col-md-3 colorlib-widget">
						<h4>Information</h4>
						<p>
							<ul class="colorlib-footer-links">
								<li><a href="#"><i class="icon-check"></i> Home</a></li>
								<li><a href="#"><i class="icon-check"></i> Gallery</a></li>
								<li><a href="#"><i class="icon-check"></i> About</a></li>
								<li><a href="#"><i class="icon-check"></i> Blog</a></li>
								<li><a href="#"><i class="icon-check"></i> Contact</a></li>
								<li><a href="#"><i class="icon-check"></i> Privacy</a></li>
							</ul>
						</p>
					</div>

					<div class="col-md-3 colorlib-widget">
						<!-- <h4>Recent Blog</h4> -->
						<!-- <div class="f-blog">
							<a href="blog.html" class="blog-img" style="background-image: url(landing-page/images/blog-1.jpg);">
							</a>
							<div class="desc">
								<h2><a href="blog.html">Photoshoot Technique</a></h2>
								<p class="admin"><span>30 March 2018</span></p>
							</div>
						</div>
						<div class="f-blog">
							<a href="blog.html" class="blog-img" style="background-image: url(landing-page/images/blog-2.jpg);">
							</a>
							<div class="desc">
								<h2><a href="blog.html">Camera Lens Shoot</a></h2>
								<p class="admin"><span>30 March 2018</span></p>
							</div>
						</div>
						<div class="f-blog">
							<a href="blog.html" class="blog-img" style="background-image: url(landing-page/images/blog-3.jpg);">
							</a>
							<div class="desc">
								<h2><a href="blog.html">Imahe the biggest photography studio</a></h2>
								<p class="admin"><span>30 March 2018</span></p>
							</div>
						</div> -->
					</div>

					<div class="col-md-3 colorlib-widget">
						<h4>Contact Info</h4>
						<ul class="colorlib-footer-links">
							<!-- <li>291 South 21th Street, <br> Suite 721 New York NY 10016</li> -->
							<li><a href="tel://0040742608858"><i class="icon-phone"></i> + 40 742 60 88 58</a></li>
							<li><a href="mailto:info@yoursite.com"><i class="icon-envelope"></i> contact@recruthor.com</a></li>
							<li><a href="#"><i class="icon-location4"></i> recruthor.com</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- <div class="copy">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
								Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a>, <a href="http://pexels.com/" target="_blank">Pexels</a>
							</p>
						</div>
					</div>
				</div>
			</div> -->
		</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="/landing-page/js-gotop"><i class="icon-arrow-up2"></i></a>
	</div>

	<!-- jQuery -->
	<script src="/landing-page/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="/landing-page/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="/landing-page/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="/landing-page/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="/landing-page/js/jquery.stellar.min.js"></script>
	<!-- YTPlayer -->
	<script src="/landing-page/js/jquery.mb.YTPlayer.min.js"></script>
	<!-- Owl carousel -->
	<script src="/landing-page/js/owl.carousel.min.js"></script>
	<!-- Magnific Popup -->
	<script src="/landing-page/js/jquery.magnific-popup.min.js"></script>
	<script src="/landing-page/js/magnific-popup-options.js"></script>
	<!-- Counters -->
	<script src="/landing-page/js/jquery.countTo.js"></script>
	<!-- Main -->
	<script src="/landing-page/js/main.js"></script>

	</body>
</html>
