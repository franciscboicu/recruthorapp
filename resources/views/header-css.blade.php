<link rel="stylesheet" href="{{ asset('theme/fonts/open-sans/style.min.css') }}"> <!-- common font  styles  -->
<link rel="stylesheet" href="{{ asset('theme/fonts/universe-admin/style.css') }}"> <!-- universeadmin icon font styles -->
<link rel="stylesheet" href="{{ asset('theme/vendor/flatpickr/flatpickr.min.css') }}"> <!-- original flatpickr plugin (datepicker) styles -->
<link rel="stylesheet" href="{{ asset('theme/vendor/simplebar/simplebar.css') }}"> <!-- original simplebar plugin (scrollbar) styles  -->
<link rel="stylesheet" href="{{ asset('theme/vendor/tagify/tagify.css') }}"> <!-- styles for tags -->
<link rel="stylesheet" href="{{ asset('theme/vendor/tippyjs/tippy.css') }}"> <!-- original tippy plugin (tooltip) styles -->
<link rel="stylesheet" href="{{ asset('theme/vendor/select2/css/select2.min.css') }}"> <!-- original select2 plugin styles -->
<link rel="stylesheet" href="{{ asset('theme/vendor/bootstrap/css/bootstrap.min.css') }}"> <!-- original bootstrap styles -->
<link rel="stylesheet" href="{{ asset('theme/css/style-g.min.css') }}" id="stylesheet"> <!-- universeadmin styles -->
<link rel="stylesheet" href="{{ asset('theme/vendor/sumo-select/sumoselect.min.css') }}">
<link rel="stylesheet" href="{{ asset('theme/css/jquery.growl/stylesheets/jquery.growl.css') }}">
