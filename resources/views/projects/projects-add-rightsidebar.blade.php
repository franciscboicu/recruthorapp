<div class="main-container">
  <div class="row">
    <div class="col-md-12">
      <label for="project-page">Partner you are recruiting for</label>
    </div>

  </div>
  <hr/>
  <div class="row">
    <div class="col-md-12">
      <select  placeholder="Contact Source" name="partner_id" class="form-control">
        <option value="1"> Something </option>
        @foreach($companies as $company)
            <option @if(isset($project) && $project->partner_id == $company->id) selected @endif value="{{$company->id}}"> {{ $company->name }} </option>
        @endforeach
      </select>
    </div>
  </div>
</div>

{{-- <div class="main-container">
  <div class="row">
    <div class="col-md-10">
      <label for="project-page">Show this Project on Organization page</label>
    </div>
    <div class="col-md-2">
      <label class="switch switch--success">
        <input type="checkbox" name="">
        <span class="switch-slider">
          <span class="switch-slider__on"></span>
          <span class="switch-slider__off"></span>
        </span>
      </label>
    </div>
  </div>
  <hr/>
  <div class="row">
    <div class="col-md-10">
      <label for="project-page">Create a landing page (campaign) for this project</label>
    </div>
    <div class="col-md-2">
      <label class="switch switch--success">
        <input type="checkbox" name="has_form">
        <span class="switch-slider">
          <span class="switch-slider__on"></span>
          <span class="switch-slider__off"></span>
        </span>
      </label>
    </div>
  </div>
</div> --}}
{{--
<div class="main-container">
  <div class="row">
    <div class="col-md-10">
      <label for="project-page">Create a form for online applications</label>
    </div>
    <div class="col-md-2">
      <label class="switch switch--success">
        <input type="checkbox" >
        <span class="switch-slider">
          <span class="switch-slider__on"></span>
          <span class="switch-slider__off"></span>
        </span>
      </label>
    </div>
  </div>
  <hr/>
  <div class="row">
    <div class="col-md-10">
      <label for="project-page">Allow candidates to apply directly with Linkedin</label>
    </div>
    <div class="col-md-2">
      <label class="switch switch--success">
        <input type="checkbox" >
        <span class="switch-slider">
          <span class="switch-slider__on"></span>
          <span class="switch-slider__off"></span>
        </span>
      </label>
    </div>
  </div>
</div> --}}

<div class="main-container">
  <h3>Job Facilities</h3>
  <p><small>*Only options that are switched on will appear in Project details</small></p>

  @foreach($projectFacilities as $pfKey => $projectFacility)
  <div class="row">
    <div class="col-md-10">
      <label for="project-page">{{$projectFacility['label']}} </label>
    </div>
    <div class="col-md-2">
      <label class="switch switch--success">
        <input type="checkbox" name="{{$pfKey}}" @if(isset($project->$pfKey) && $project->$pfKey == 'on') checked @endif >
        <span class="switch-slider">
          <span class="switch-slider__on"></span>
          <span class="switch-slider__off"></span>
        </span>
      </label>
    </div>
  </div>
  <hr/>
  @endforeach

</div>
