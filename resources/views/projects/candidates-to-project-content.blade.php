<div class="tab-content" id="content" data-candidate-id="{{$project->id}}" data-project-id="{{$project->id}}">

  <!-- <div class="modal-header has-border row">

  </div> -->

  <div class="tab-pane fade show active mt-5" id="modal-notes">
    <div class="row">
      <div class="col-md-1">
        <img class="" src="/theme/img/document.png" alt="">
      </div>
      <div class="col-md-10">
        <div class="form-group">
          <textarea class="form-control" id="note" rows="1" placeholder="Search by name or skills"></textarea>
        </div>
      </div>
      <div class="col-md-1">
        <button class="btn btn-success btn-block btn-icon" data-candidate-id="{{$project->id}}" data-project-id="{{$project->id}}" id="save-note">
          <span class="ua-icon ua-icon-send"></span>
        </button>
      </div>
    </div>


    @if(count($candidates) > 0)
    <ul class="list-unstyled">
      @foreach($candidates as $candidate)
      <li class="media">
        <img class="mr-3 mt-2" src="/theme/img/users/user-1.png" class="rounded-circle" alt="Jane Doe">
        <div class="media-body">
          <h5 class="mt-0 mb-1"><strong>{{$candidate->fullname}}</strong></h5>
          <p>{{ $candidate->getMainSkillsList(true) }}</p>
          <p>Has only: </p>
          @if(count($candidate->getOnlyMatchedSkills($project->getMainSkillsList())) > 0)
          @foreach($candidate->getOnlyMatchedSkills($project->getMainSkillsList()) as $skill)
            {{$skill}}
          @endforeach
          @endif
          <button class="btn btn-warning btn-sm delete-note float-right" stype="padding:0px;" data-note-id="">
            <span class="ua-icon ua-icon-info"></span>
          </button>

        </div>
        <hr/>
      </li>
      <li class="media">
        <hr/>
      </li>
      @endforeach
    </ul>
    @else
      <p>This candidate has 0 notes added in this project.</p>
    @endif

  </div>

  <div class="tab-pane fade" id="modal-tests">
    @include('general.to-do-small')
  </div>

  <div class="tab-pane fade" id="modal-notifications">

  </div>
</div>

<script>
$(document).ready(function(){

  $(".change-stage").click(function(){
    stage = $(this).data("stage");
    candidateId = $("#content").data("candidate-id");
    projectId   = $("#content").data( "project-id" );

    $('.change-stage').removeClass('btn-success active');
    $(this).addClass('btn-success active');

    $.ajax({
          type: "POST",
          url: "{{ route('pipeline/status/update') }}",
          data: {"candidate_id": candidateId, "project_id": projectId, "_token": '{{ csrf_token() }}', 'stage': stage },

          success: function(response){
              if(response.success == 0){
                alert(response.message);
              }

              if(response.success == 1){
                 //location.reload();
                 window.reloadPageAfterPopupClose = true;
              }
          },
          dataType: false
        });

  });


  $(".modal-title").text('{{$project->title}}');
  $(".delete-note").click(function(){
    noteId = $(this).data("note-id");
    $.ajax({
        type: "POST",
        url: "{{ route('projects/candidate/note/delete') }}",
        data: {"note_id": noteId, "_token": '{{ csrf_token() }}'},
        success: function(response){

            if(response.success == 0){
              alert(response.message);
            }

            if(response.success == 1){
               $("#save-note").closest('.modal-body').load("{{route('projects/candidate/info',['project_id' => $project->id, 'project_id_' => $project->id])}}", function(){
                 console.log('loaded');
               });
            }
        },
        dataType: false
      });
  });

  $("#save-note").click(function(){

    candidateId = $(this).data("candidate-id");
    projectId   = $(this).data( "project-id" );
    note        = $("#note").val();

    if("" === note) {
      alert("Please add a note");
      return false;
    }

    $.ajax({
        type: "POST",
        url: "{{ route('projects/candidate/note/save') }}",
        data: {"candidate_id": candidateId, "project_id": projectId, "_token": '{{ csrf_token() }}', 'note': note },
        success: function(response){

            if(response.success == 0){
              alert(response.message);
            }

            if(response.success == 1){
               $("#save-note").closest('.modal-body').load("{{route('projects/candidate/info',['project_id' => $project->id, 'candidate_id' => $project->id])}}", function(){
                 console.log('loaded');
               });
            }
        },
        dataType: false
      });
  });


});

</script>
