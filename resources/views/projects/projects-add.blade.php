@extends('layouts.theme')

@section('content')


<div class="container-fluid">
  <!-- <div class="page-content__header">
    <div>
      <h2 class="page-content__header-heading">Form Inputs</h2>
    </div>
  </div> -->

  <div class="main-container">
    <div class="row">
      <div class="col-md-8">

        @if(isset($project))
        <h2 class="h2" style="margin-bottom: 0px;">Edit: {{ $project->title }}</h2>
        @else
          <h2 class="h2" style="margin-bottom: 0px;">Creating a new Project</h2>
        @endif

      </div>
      <div class="col-md-4">
        @if(isset($project))
        <div class="btn-group btn-collection mr-3 float-right">
          {{-- <a href="" class="btn btn-success active">General</a>
          <a href="" class="btn btn-secondary ">Settings</a>
          <a href="" class="btn btn-secondary ">Financial</a> --}}
          <a href="{{route('projects/read',['id' => $project->id])}}" class="btn btn-warning">Go to work space</a>
        </div>
        @endif
      </div>
    </div>

  </div>

  @if(isset($project))
  <form action="{{ route('projects/edit',['id' => $project->id]) }}" method="POST">
  @else
    <form action="{{ route('projects/add') }}" method="POST">
  @endif

    <div class="row">
      <div class="col-md-8">
        <div class="main-container">

          <div class="row">
            <div class="col-lg-6">
              <h4>Project title</h4>
              <div class="form-group input-group-lg">
                <input type="text" placeholder="Ex: Senior PHP Developer" name="title" class="form-control" value="@if(isset($project)){{ $project->title }}@endif">
              </div>
            </div>
            <div class="col-lg-6">
              <label for="read-only">Department</label>
              <select  placeholder="Contact Source" name="department" class="form-control">
                @foreach($departments as $departmentKey => $departmentLabel)
                    <option @if(isset($project) && $project->source == $departmentKey) selected @endif value="{{$departmentKey}}"> {{ $departmentLabel }} </option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <h4>Country</h4>
              <div class="form-group input-group-lg">
                <input type="text" placeholder="Ex: Senior PHP Developer" name="country_name" class="form-control" value="@if(isset($project)){{ $project->country_name }}@endif">
              </div>
            </div>
            <div class="col-lg-6">
              <h4>City</h4>
              <div class="form-group input-group-lg">
                <input type="text" placeholder="Ex: Senior PHP Developer" name="city_name" class="form-control" value="@if(isset($project)){{ $project->city_name }}@endif">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <label for="read-only">Type</label>
              <select  placeholder="Contact Source" name="employment_type" class="form-control">
                @foreach($types as $typeKey => $typeLabel)
                    <option @if(isset($project) && $project->source == $typeKey) selected @endif value="{{$typeKey}}"> {{ $typeLabel }} </option>
                @endforeach
              </select>
            </div>
            <div class="col-lg-6">
              <label for="read-only">Industry</label>
              <select  placeholder="Contact Source" name="industry" class="form-control">
                @foreach($industries as $industryKey => $industryLabel)
                    <option @if(isset($project) && $project->source == $industryKey) selected @endif value="{{$industryKey}}"> {{ $industryLabel }} </option>
                @endforeach
              </select>
            </div>
          </div>

          <br/>

          <div class="row">
            <div class="col-lg-6">
              <label for="read-only">Education Level</label>
                <select  placeholder="Contact Source" name="education_level" class="form-control">
                @foreach($educations as $educationKey => $educationLabel)
                    <option @if(isset($project) && $project->source == $educationKey) selected @endif value="{{$educationKey}}"> {{ $educationLabel }} </option>
                @endforeach
              </select>
            </div>
            <div class="col-lg-6">
              <label for="read-only">Experience Needed</label>
              <select  placeholder="Contact Source" name="experience_level" class="form-control">
                @foreach($xpLevels as $xpKey => $xpLabel)
                    <option @if(isset($project) && $project->source == $xpKey) selected @endif value="{{$xpKey}}"> {{ $xpLabel }} </option>
                @endforeach
              </select>
            </div>
          </div>

          <br/>

          <div class="row">
            <div class="col-lg-12">
              <label for="main-skills">Wanted Skills</label>
              <div class="tag-editor"> <!-- need wrap -->
                <textarea id="main-skills" name="main_skills" placeholder='Write some tags'>
                  @if(isset($project)) {{ $project->main_skills }} @endif
                </textarea>
              </div>
            </div>
          </div>

          <br/>

          <div class="row">
            <div class="col-lg-12">
              <label for="main-skills">Job Description</label>
                <div class="form-group">
                  <textarea rows="3" name="job_description" placeholder="Try to keep this things simple and easy to read and understand" class="form-control">@if(isset($project)) {{ $project->job_description }} @endif</textarea>
                </div>
            </div>
          </div>

          <br/>

          <div class="row">
            <div class="col-lg-12">
              <label for="main-skills">Desired candidate description</label>
                <div class="form-group">
                  <textarea rows="3" name="job_requirements" placeholder="Try to keep this things simple and easy to read and understand" class="form-control">@if(isset($project)) {{ $project->job_requirements }} @endif</textarea>
                </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-3">
              <h5>Lowest salary interval</h5>
              <div class="form-group input-group-lg">
                <input type="text" placeholder="" name="min_salary" class="form-control" value="@if(isset($project)){{ $project->min_salary }}@else 0 @endif">
              </div>
            </div>
            <div class="col-lg-3">
              <h5>Highest salary interval</h5>
              <div class="form-group input-group-lg">
                <input type="text" placeholder="" name="max_salary" class="form-control" value="@if(isset($project)){{ $project->max_salary }}@else 0 @endif">
              </div>
            </div>
            <div class="col-lg-4">
              <h5>Currency</h5>
              <select  placeholder="Contact Source" name="partner_id" class="form-control">
                @foreach($currencies as $currency)
                  <option @if(isset($project) && $project->salary_currency == $currency) selected @endif value="{{$currency}}"> {{ $currency }} </option>
                @endforeach
                </select>
            </div>
            <div class="col-lg-2">
                <h5>Total  Openings</h5>
                <div class="form-group input-group-lg">
                  <input type="text" placeholder="Total openings for this project" name="total_openings" class="form-control" value="@if(isset($project)){{ $project->total_openings }}@else 1 @endif">
                </div>
            </div>
          </div>

        </div>
      </div>
      <div class="col-md-4">

        @include('projects.projects-add-rightsidebar')

      </div>
    </div>


  @csrf

  <div class="main-container">
    <div class="row">
      <div class="col-lg-10">
        <p><small>This section helps you create a new project. Projects are the second important things in this software, because Candidates revolve around them.</small></p>
      </div>
      <div class="col-lg-2">
        @if(isset($project))
        <button  class="float-right btn btn-success"> Update project </button>
        @else
          <button  class="float-right btn btn-success"> Add project </button>
        @endif

    </div>
    </div>
  </div>

  <!-- <div id="sub">Save</div> -->
</form>

</div>


@endsection

@section('script')
  <script>

  (function ($) {
    'use strict';

    $(document).ready(function() {
      var mainSkills = new Tagify($('#main-skills').get(0));
      var secondarySkills = new Tagify($('#secondary-skills').get(0));
      //
      // $('#selectbox-ex1').SumoSelect();
    });
  })(jQuery);


  </script>
@endsection
