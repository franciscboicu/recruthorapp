@extends('layouts.theme')

@section('content')

<style>
.m-tasks__column{
  padding:1px;
  width:220px;
  min-width:220px;
  /* border-right:1px solid #ccc; */
}

.container-body{
  padding:3px;
  padding-top:10px;
}

.m-tasks__item-name{
  margin-bottom:0px;
  font-size:15px;
  font-weight:thin;
}

.m-tasks__column-name{
  font-weight:bold;
  font-size:18px;
}

.list-group-item{
  border: 1px dashed #ccc; margin-bottom:3px; cursor:pointer;
  border-radius:5px;
}

.list-group{
  background-color: #ecf2f6;
  border-radius:5px;
  padding:5px;
}

/* .m-tasks__column-header{
  border-bottom:1px solid #ccc;
} */
</style>


<div class="container-fluid m-tasks">
  <div class="main-container m-tasks__container container-heading-bordered">

    <h2 class="container-heading">
      <span>Project Progress</span>
      <span class="container-heading-controls">
        <a href="#" class="ua-icon-action-plus container-heading-control"></a>
      </span>
    </h2>

    <div class="container-body m-tasks__columns">

      @foreach($flow as $flowKey => $flowData)

      <!-- m-tasks__column- - pending -->
      <div class="m-tasks__column m-tasks__column list-group ml-2 " id="{{$flowKey}}">

        <div class="m-tasks__column-header">
          <!-- <span class="m-tasks__column-icon ua-icon-task-label"></span> -->
          <span class="m-tasks__column-name ml-2">{{ $flowData['label']}}</span>
        </div>

        <div class="m-tasks__items">

          @foreach($projectCandidates as $_candidate)
            @if($_candidate->stage === $flowKey)
              @include('projects.candidate-box')
            @endif
          @endforeach

        </div>

        @if($flowKey == 'applied')

          <button class="btn btn-outline-success btn-block iconfont icon-left btn-lg m-tasks__add-card"
              data-toggle="modal"
              data-href="{{ route('projects/getCandidatesForProject',['id'=> $project->id]) }}"
              data-target="#add-candidates-to-project"
              data-project-id="{{ $project->id }}"
              type="button">
              Add candidates<span class="btn-icon ua-icon-plus"></span>
            </button>

        @endif

      </div>


      @endforeach

    </div>

  </div>
</div>


  @include('projects.candidate-info-popup')
  @include('projects.candidates-to-project')

@endsection

@section('script')

<script>
  $(document).ready(function(){

    window.reloadPageAfterPopupClose == false;


    function terminateState(changeHow, candidateId, projectId){

      $.ajax({
            type: "POST",
            url: "{{ route('projects/candidate/pipeline/state') }}",
            data: {"candidate_id": candidateId, "project_id": projectId, "_token": '{{ csrf_token() }}', 'change_how': changeHow },

            success: function(response){
                if(response.success == 0) {
                  alert(response.message);
                }

                if(response.success == 1) {
                   location.reload();
                }
            },
            dataType: false
          });

    }

    $('.terminate-positive').click(function(){
      candidateId = $(this).data("candidate-id");
      projectId   = $(this).data( "project-id" );
      return terminateState('positive', candidateId, projectId);
    });

    $('.terminate-negative').click(function(){
      candidateId = $(this).data("candidate-id");
      projectId   = $(this).data( "project-id" );
      return terminateState('negative', candidateId, projectId);
    });

    //modal settings
    $('#modal-settings').modal({
            backdrop: 'static',
            keyboard: false,
            show: false
    });

    $('#add-candidates-to-project').modal({
            backdrop: 'static',
            keyboard: false,
            show: false
    });

    //add-candidates-to-project

    //load the modal with dynamic content
    $('#modal-settings').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); //Clicked btn
      var url = button.data("href");
      var modal = $(this);
      modal.find('.modal-body').load(url, function(){
        //when loaded
      });
    });

    $('#add-candidates-to-project').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); //Clicked btn
      var url = button.data("href");
      var modal = $(this);
      modal.find('.modal-body').load(url, function(){
        //when loaded
      });
    });

    $('#modal-settings').on('hidden.bs.modal', function () {

      //if state of window.needsReload changed, switch back and reload
      if(window.reloadPageAfterPopupClose == true){
        window.reloadPageAfterPopupClose == false;
        location.reload();
      }

    });

    $('#add-candidates-to-project').on('hidden.bs.modal', function () {

      //if state of window.needsReload changed, switch back and reload
      if(window.reloadPageAfterPopupClose == true){
        window.reloadPageAfterPopupClose == false;
        location.reload();
      }

    });



  });
</script>
@endsection
