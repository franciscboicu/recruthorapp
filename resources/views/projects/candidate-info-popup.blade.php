<style>
.modal-title{
  font-size:22px;
}
</style>
<div id="modal-settings" class="modal fade custom-modal-tabs"  style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header has-border">
        <h6 class="modal-title"></h6>
        <div class="btn-group btn-collection nav custom-modal__header-tabs" role="group" aria-label="Candidate to Project info">
          <button class="btn btn-secondary nav-item nav-link active" type="button" data-toggle="tab" data-target="#modal-notes">Notes</button>
          <button class="btn btn-secondary nav-item nav-link" type="button" data-toggle="tab" data-target="#modal-tests">Tests</button>
          <button class="btn btn-secondary nav-item nav-link" type="button" data-toggle="tab" data-target="#modal-notifications">Notifications</button>
        </div>
        <button type="button" class="close custom-modal__close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="ua-icon-modal-close"></span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <!-- <div class="modal-footer modal-footer--center">
        <button type="button" class="btn btn-outline-info" data-dismiss="modal" >Cancel</button>
        <button class="btn btn-info" type="button">Save</button>
      </div> -->
    </div>
  </div>
  </div>
