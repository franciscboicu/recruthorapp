@if($_candidate->candidate)
<div class="m-tasks__item list-group-item candidate-box" data-candidate-id="{{$_candidate->candidate_id}}">

  <div class="dropdown control-dropdown m-tasks__item-control">
    <div class="btn-group btn-collection btn-icon-group btn-group-lg float-right" style="background-color: #fff;">
      <button class="btn btn-outline-danger terminate-negative" data-project-id="{{ $project->id }}"  data-candidate-id="{{ $_candidate->candidate_id }}" type="button"><span class="btn-icon ua-icon-archive"></span></button>
      <button class="btn btn-outline-danger" data-toggle="modal" data-href="{{route('projects/candidate/info',['project_id' => $_candidate->project_id, 'candidate_id' => $_candidate->candidate_id])}}" data-target="#modal-settings" data-project-id="{{ $project->id }}"  data-candidate-id="{{ $_candidate->candidate_id }}" type="button"><span class="btn-icon ua-icon-dots"></span></button>
      <button class="btn btn-outline-success active terminate-positive" data-project-id="{{ $project->id }}"  data-candidate-id="{{ $_candidate->candidate_id }}" type="button"><span class="btn-icon ua-icon-check-alt"></span></button>
    </div>
  </div>

  <h6 class="m-tasks__item-name">{{ $_candidate->candidate->fullname }}</h6>
  <!-- <div class="m-tasks__item-desc">
    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
  </div> -->
<!-- <span class="m-tasks__item-date">12 April 2018</span> -->
  <!-- <span class="m-tasks__item-priority ua-icon-task-bell m-tasks__item-priority--medium"></span> -->
</div>
@endif
