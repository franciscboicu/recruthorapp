@extends('layouts.theme')

@section('content')

<style>
.m-tasks__column{
  padding:1px;
  width:220px;
  min-width:220px;
  margin-right:5px;
  /* border-right:1px solid #ccc; */
}

.container-body{
  padding:3px;
  padding-top:10px;
}

.m-tasks__item-name{
  margin-bottom:0px;
}

.list-group-item{
  border: 1px dashed #ccc; margin-bottom:3px; cursor:pointer;
}

.m-grid__item {
  /* width: calc(100% - 0); */
  width:100%;
}
/* .m-tasks__column-header{
  border-bottom:1px solid #ccc;
} */
</style>

<div class="container-fluid container-fh m-grid">
  <div class="main-container main-container--empty container-fh__content">
    <div class="container-header">
      <h2 class="container-heading">Users</h2>
      <div class="container-header-controls">
        <div class="input-group input-group-icon icon-right container-header-control m-grid__search">
          <input class="form-control" type="text" placeholder="Search...">
          <span class="input-icon ua-icon-search"></span>
        </div>
        <div class="dropdown container-header-control">
          <a class="btn btn-info dropdown-toggle" href="#" data-toggle="dropdown">
            Action
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </div>
        <a href="#" class="btn btn-info container-header-control">Create user</a>
      </div>
    </div>
    <div class="m-grid__body">
      <div class="m-grid__body-scrollpane js-scrollable" data-simplebar="init"><div class="simplebar-track vertical" style="visibility: hidden;"><div class="simplebar-scrollbar" style="top: 2px; height: 206px;"></div></div><div class="simplebar-track horizontal" style="visibility: hidden;"><div class="simplebar-scrollbar"></div></div><div class="simplebar-scroll-content" style="padding-right: 15px; margin-bottom: -30px;"><div class="simplebar-content" style="padding-bottom: 15px; margin-right: -15px;">
        <div class="m-grid__items">


          @for($i=1;$i<=30;$i++)

          <div class="m-grid__item ">
            <div class="dropdown no-arrow control-dropdown">
              <span class="dropdown-toggle ua-icon-dots-vertical" data-toggle="dropdown"></span>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Edit</a>
                <a class="dropdown-item" href="#">Delete</a>
              </div>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" checked="">
              <label class="custom-control-label"></label>
            </div>
            <div class="d-flex">
              <img src="img/users/user-5.png" alt="" class="m-grid__item-avatar rounded-circle" width="34" height="34">
              <div class="m-grid__item-info">
                <a href="#" class="m-grid__item-name">Lori Fisher</a>
                <span class="m-grid__item-email">lfisher@example.com</span>
              </div>
            </div>
            <div class="m-grid__item-labels">
              <span class="m-grid__item-label m-grid__item-label--active">Active 12</span>
              <span class="m-grid__item-label m-grid__item-label--deactivated">Deactivated 20</span>
            </div>
          </div>

          @endfor


        </div>
      </div></div></div>
    </div>

  </div>
</div>
@endsection

@section('script')

<script>
  $(document).ready(function(){

    function terminateState(changeHow, candidateId, projectId){

      $.ajax({
            type: "POST",
            url: "{{ route('projects/candidate/pipeline/state') }}",
            data: {"candidate_id": candidateId, "project_id": projectId, "_token": '{{ csrf_token() }}', 'change_how': changeHow },

            success: function(response){
                if(response.success == 0){
                  alert(response.message);
                }

                if(response.success == 1){
                   location.reload();
                }
            },
            dataType: false
          });

    }
    $('.terminate-positive').click(function(){
      candidateId = $(this).data("candidate-id");
      projectId   = $(this).data( "project-id" );
      return terminateState('positive', candidateId, projectId);
    });

    $('.terminate-negative').click(function(){
      candidateId = $(this).data("candidate-id");
      projectId   = $(this).data( "project-id" );
      return terminateState('negative', candidateId, projectId);
    });

  });
</script>
@endsection
