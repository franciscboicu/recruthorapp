@extends('layouts.theme')

@section('content')


<div class="container-fluid m-invoices scroll">
  <div class="main-container m-invoices__container">
    <div class="m-invoices__tables">
      <div class="table-header">
        @if(0 < $projectsCount)
        <h4 class="table-header__heading">Projects</h4>
        <div class="table-header__controls">
            <a href="{{ route('projects/add') }}" class="btn btn-success "> Create new project</a>
        </div>
        @endif
      </div>
      @if(0 < $projectsCount)
      <table class="table table-hover table__actions">
        <tbody>
          @foreach($projects as $project)
          <tr>
            <td><a href="{{route('projects/read', ['id' => $project->id ])}}" class="widget-notifications__user">{{ $project->title }}</a></td>
            <td>
             @for($i=1;$i<=count($project->candidates);$i++)
             <span class="ua-icon-user-outline" style="color: green;"></span>
                <!-- <span class="table__tag table__tag--green"></span> -->
             @endfor
            </td>
            <td>{{ $project->getShortCity() }}, {{ $project->getShortCountry() }}</td>
            <td>
              @foreach($project->getMainSkillsList() as $mainSkill)
                <span class="badge badge-pill badge-dark badge-small"><small>{{ $mainSkill }}</small></span>
              @endforeach
            </td>
            <td>
              <a href="{{ route('projects/edit', ['id' => $project->id]) }}" class="table__cell-actions-item table__cell-actions-icon">
                <span class="ua-icon-edit-outline"></span>
              </a>
              <a href="#" class="table__cell-actions-item table__cell-actions-icon">
                <span class="ua-icon-send"></span>
              </a>
              <a href="{{ route('projects/delete', ['id' => $project->id]) }}" class="table__cell-actions-item table__cell-actions-icon">
                <span class="ua-icon-remove"></span>
              </a>
              <a href="#" class="table__cell-actions-item table__cell-actions-icon">
                <span class="ua-icon-info"></span>
              </a>
            </td>

          </tr>
          @endforeach
        </tbody>
      </table>
      @else
      <div class="row" style="margin-top:200px;">
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
          <img src="{{asset('theme/img/sad.png')}}" width="128px" height="128px"/>
          <br/>
          <h2>Make this guy happy!</h2>
          and <a href="{{ route('projects/add') }}" class="btn btn-success ">
            add your first project
          </a>
        </div>
        <div class="col-md-4"></div>
      </div>
      @endif

    </div>

  </div>
</div>


@endsection
