<div class="tab-content" id="content" data-candidate-id="{{$candidateProject->candidate_id}}" data-project-id="{{$candidateProject->project_id}}">

  <div class="modal-header has-border row  card card-body bg-light">
    <div class="btn-group d-flex btn-collection  nav custom-modal__header-tabs" role="group" aria-label="Candidate to Project info">
      <button class="btn change-stage  nav-item nav-link @if($candidateProject->is('applied')) btn-success active @endif" data-stage="applied">Applied</button>
      <button class="btn change-stage  nav-item nav-link @if($candidateProject->is('phone')) btn-success active @endif" data-stage="phone">Phone</button>
      <button class="btn change-stage  nav-item nav-link @if($candidateProject->is('interview')) btn-success active @endif" data-stage="interview">Interview</button>
      <button class="btn change-stage  nav-item nav-link @if($candidateProject->is('evaluation')) btn-success active @endif" data-stage="evaluation">Evaluation</button>
      <button class="btn change-stage  nav-item nav-link @if($candidateProject->is('offer')) btn-success active @endif" data-stage="offer">Offer</button>
    </div>
  </div>

  <div class="tab-pane fade show active mt-5 " id="modal-notes">
    <div class="row ">
      <div class="col-md-1">
        <img class="" src="/theme/img/document.png" alt="">
      </div>
      <div class="col-md-10 ">
        <div class="form-group">
          <textarea class="form-control" id="note" rows="1" placeholder="Add a short note"></textarea>
        </div>
      </div>
      <div class="col-md-1">
        <button class="btn btn-success btn-block btn-icon" data-candidate-id="{{$candidateProject->candidate_id}}" data-project-id="{{$candidateProject->project_id}}" id="save-note">
          <span class="ua-icon ua-icon-send"></span>
        </button>
      </div>
    </div>


    @if(count($projectNotes) > 0)
    <ul class="list-unstyled">
      @foreach($projectNotes as $projectNote)
      <li class="media">
        <img class="mr-3 mt-2" src="/theme/img/users/user-1.png" class="rounded-circle" alt="Jane Doe">
        <div class="media-body">
          <h5 class="mt-0 mb-1">{{$projectNote->name}}</h5>
          {{$projectNote->note}}

          @if($projectNote->added_by_user_id == $currentUser->id)
          <button class="btn btn-warning btn-sm delete-note float-right" stype="padding:0px;" data-note-id="{{$projectNote->id}}">
            <span class="ua-icon ua-icon-trash"></span>
          </button>
          @endif
        </div>
        <hr/>
      </li>
      <li class="media">
        <hr/>
      </li>
      @endforeach
    </ul>
    @else
      <p>This candidate has 0 notes added in this project.</p>
    @endif

  </div>

  <div class="tab-pane fade" id="modal-tests">
    @include('general.to-do-small')
  </div>

  <div class="tab-pane fade" id="modal-notifications">

  </div>
</div>

<script>
$(document).ready(function(){

  $(".change-stage").click(function(){
    stage = $(this).data("stage");

    candidateId = $("#content").data("candidate-id");
    projectId   = $("#content").data( "project-id" );

    $('.change-stage').removeClass('btn-success active');
    $(this).addClass('btn-success active');

    $.ajax({
          type: "POST",
          url: "{{ route('pipeline/status/update') }}",
          data: {"candidate_id": candidateId, "project_id": projectId, "_token": '{{ csrf_token() }}', 'stage': stage },

          success: function(response){
              if(response.success == 0){
                alert(response.message);
              }

              if(response.success == 1){
                 window.reloadPageAfterPopupClose = true;
              }
          },
          dataType: false
        });

  });


  $(".modal-title").text('{{$candidate->fullname}}');
  $(".delete-note").click(function(){
    noteId = $(this).data("note-id");
    $.ajax({
        type: "POST",
        url: "{{ route('projects/candidate/note/delete') }}",
        data: {"note_id": noteId, "_token": '{{ csrf_token() }}'},
        success: function(response){

            if(response.success == 0){
              alert(response.message);
            }

            if(response.success == 1){
               $("#save-note").closest('.modal-body').load("{{route('projects/candidate/info',['project_id' => $candidateProject->project_id, 'candidate_id' => $candidateProject->candidate_id])}}", function(){
                 console.log('loaded');
               });
            }
        },
        dataType: false
      });
  });

  $("#save-note").click(function(){

    candidateId = $(this).data("candidate-id");
    projectId   = $(this).data( "project-id" );
    note        = $("#note").val();

    if("" === note) {
      alert("Please add a note");
      return false;
    }

    $.ajax({
        type: "POST",
        url: "{{ route('projects/candidate/note/save') }}",
        data: {"candidate_id": candidateId, "project_id": projectId, "_token": '{{ csrf_token() }}', 'note': note },
        success: function(response){

            if(response.success == 0){
              alert(response.message);
            }

            if(response.success == 1){
               $("#save-note").closest('.modal-body').load("{{route('projects/candidate/info',['project_id' => $candidateProject->project_id, 'candidate_id' => $candidateProject->candidate_id])}}", function(){
                 console.log('loaded');
               });
            }
        },
        dataType: false
      });
  });


});

</script>
