<div class="navbar navbar-light navbar-expand-lg">
<button class="sidebar-toggler" type="button">
  <span class="ua-icon-sidebar-open sidebar-toggler__open"></span>
  <span class="ua-icon-alert-close sidebar-toggler__close"></span>
</button>

<a class="navbar-brand" href="/"><img src="/theme/img/logo.png" alt=""/></a>
<a class="navbar-brand-sm" href="/"><img src="/theme/img/logo-sm.png" alt=""/></a>

<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse">
  <span class="ua-icon-navbar-open navbar-toggler__open"></span>
  <span class="ua-icon-alert-close navbar-toggler__close"></span>
</button>

<div class="collapse navbar-collapse" id="navbar-collapse">
  <div class="navbar-search">
    <div class="input-group iconfont icon-right">
      <input class="form-control navbar-search__input" type="text" placeholder="Search"/><span class="input-icon ua-icon-search"></span>
    </div>
  </div>
  <div class="navbar__menu mr-4 d-flex justify-content-end">
    <a href="#" class="btn btn-warning btn-sm navbar__btn-sm">Upgrade your account!</a>
  </div>
  <div class="dropdown navbar-dropdown no-arrow navbar-notify-dropdown">
    <a class="dropdown-toggle navbar-dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
      <span class="navbar-notify navbar-notify--notifications">
        <span>
          <span class="navbar-notify__icon ua-icon-bell"></span>
          <span class="navbar-notify__text">Notifications</span>
        </span>
        <!--<span class="navbar-notify__amount">3</span>-->
        <!--<span class="navbar-notify__indicator"></span>-->
      </span>
  </a>
    <div class="dropdown-menu dropdown-menu-right navbar-dropdown-menu navbar-dropdown-notifications">
      <div class="navbar-dropdown-notifications__header">
        <span>NOTIFICATIONS</span>
        <a href="#" class="navbar-dropdown-notifications__mark-read">
          Mark all as read <span class="navbar-dropdown-notifications__mark-read-icon ua-icon-arrow-circle-down"></span>
        </a>
      </div>
      <div class="navbar-dropdown-notifications__body js-scrollable">
        <!--<div class="navbar-dropdown-notifications__body-empty"><img class="navbar-dropdown-notifications__body-empty-image" src="img/empty-notifications.png" alt=""/>
          <div class="navbar-dropdown-notifications__body-empty-text">You`re up to date!</div>
        </div>-->

        <div class="navbar-dropdown-notification is-new">
          <div class="navbar-dropdown-notification__user">
            <img class="navbar-dropdown-notification__avatar rounded-circle" src="/theme/img/users/user-4.png" alt="" width="40" height="40">
            <div class="ua-icon-circle-check navbar-dropdown-notification__icon color-success"></div>
          </div>
          <div class="navbar-dropdown-notification__content">
            <a href="#" class="navbar-dropdown-notification__action-name">Antonius in Project X </a>
            <div class="navbar-dropdown-notification__action-desc">Added a <strong>Task</strong> to you in <strong>Designer Candidates</strong></div>
          </div>
          <span class="navbar-dropdown-notification__date">9:49 AM</span>
        </div>
        <div class="navbar-dropdown-notification">
          <div class="navbar-dropdown-notification__user">
            <img class="navbar-dropdown-notification__avatar rounded-circle" src="/theme/img/users/user-5.png" alt="" width="40" height="40">
            <div class="ua-icon-letter-alt navbar-dropdown-notification__icon color-danger navbar-dropdown-notification__icon--letter"></div>
          </div>
          <div class="navbar-dropdown-notification__content">
            <a href="#" class="navbar-dropdown-notification__action-name">Antonius in Project X </a>
            <div class="navbar-dropdown-notification__action-desc">Added a <strong>Task</strong> to you in <strong>Designer Candidates</strong></div>
          </div>
          <span class="navbar-dropdown-notification__date">9:49 AM</span>
        </div>
        <div class="navbar-dropdown-notification">
          <div class="navbar-dropdown-notification__user">
            <img class="navbar-dropdown-notification__avatar rounded-circle" src="/theme/img/users/user-6.png" alt="" width="40" height="40">
            <div class="ua-icon-warning navbar-dropdown-notification__icon color-warning navbar-dropdown-notification__icon--warning"></div>
          </div>
          <div class="navbar-dropdown-notification__content">
            <a href="#" class="navbar-dropdown-notification__action-name">Antonius in Project X </a>
            <div class="navbar-dropdown-notification__action-desc">Added a <strong>Task</strong> to you in <strong>Designer Candidates</strong></div>
          </div>
          <span class="navbar-dropdown-notification__date">9:49 AM</span>
        </div>
        <div class="navbar-dropdown-notification">
          <div class="navbar-dropdown-notification__user">
            <img class="navbar-dropdown-notification__avatar rounded-circle" src="/theme/img/users/user-7.png" alt="" width="40" height="40">
            <div class="ua-icon-circle-check navbar-dropdown-notification__icon color-success"></div>
          </div>
          <div class="navbar-dropdown-notification__content">
            <a href="#" class="navbar-dropdown-notification__action-name">Antonius in Project X </a>
            <div class="navbar-dropdown-notification__action-desc">Added a <strong>Task</strong> to you in <strong>Designer Candidates</strong></div>
          </div>
          <span class="navbar-dropdown-notification__date">9:49 AM</span>
        </div>
        <div class="navbar-dropdown-notification__date-separator">Yesterday</div>
        <div class="navbar-dropdown-notification">
          <div class="navbar-dropdown-notification__user">
            <img class="navbar-dropdown-notification__avatar rounded-circle" src="/theme/img/users/user-8.png" alt="" width="40" height="40">
            <div class="ua-icon-letter-alt navbar-dropdown-notification__icon color-danger navbar-dropdown-notification__icon--letter"></div>
          </div>
          <div class="navbar-dropdown-notification__content">
            <a href="#" class="navbar-dropdown-notification__action-name">Antonius in Project X </a>
            <div class="navbar-dropdown-notification__action-desc">Added a <strong>Task</strong> to you in <strong>Designer Candidates</strong></div>
          </div>
          <span class="navbar-dropdown-notification__date">9:49 AM</span>
        </div>
        <div class="navbar-dropdown-notification">
          <div class="navbar-dropdown-notification__user">
            <img class="navbar-dropdown-notification__avatar rounded-circle" src="/theme/img/users/user-9.png" alt="" width="40" height="40">
            <div class="ua-icon-warning navbar-dropdown-notification__icon color-warning navbar-dropdown-notification__icon--warning"></div>
          </div>
          <div class="navbar-dropdown-notification__content">
            <a href="#" class="navbar-dropdown-notification__action-name">Antonius in Project X </a>
            <div class="navbar-dropdown-notification__action-desc">Added a <strong>Task</strong> to you in <strong>Designer Candidates</strong></div>
          </div>
          <span class="navbar-dropdown-notification__date">9:49 AM</span>
        </div>
        <div class="navbar-dropdown-notification">
          <div class="navbar-dropdown-notification__user">
            <img class="navbar-dropdown-notification__avatar rounded-circle" src="/theme/img/users/user-10.png" alt="" width="40" height="40">
            <div class="ua-icon-circle-check navbar-dropdown-notification__icon color-success"></div>
          </div>
          <div class="navbar-dropdown-notification__content">
            <a href="#" class="navbar-dropdown-notification__action-name">Antonius in Project X </a>
            <div class="navbar-dropdown-notification__action-desc">Added a <strong>Task</strong> to you in <strong>Designer Candidates</strong></div>
          </div>
          <span class="navbar-dropdown-notification__date">9:49 AM</span>
        </div>
      </div>
      <a class="navbar-dropdown-notifications__view-all" href="#"><span class="icon ua-icon-view-all"></span><span>View all</span></a>
    </div>
  </div>
  <div class="dropdown navbar-dropdown no-arrow navbar-notify-dropdown navbar-notify-dropdown--messages">
    <a class="dropdown-toggle navbar-dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
      <span class="navbar-notify">
        <span>
          <span class="navbar-notify__icon ua-icon-envelope"></span>
          <span class="navbar-notify__text">Messages</span>
        </span>
        <span class="navbar-notify__indicator"></span>
        <!--<span class="navbar-notify__amount">5</span>-->
      </span>
    </a>
    <div class="dropdown-menu dropdown-menu-right navbar-dropdown-menu navbar-dropdown-notifications navbar-dropdown-messages">
      <div class="navbar-dropdown-notifications__header"><span>MESSAGES</span>
        <a href="#" class="navbar-dropdown-notifications__mark-read">
          Mark all as read <span class="navbar-dropdown-notifications__mark-read-icon ua-icon-arrow-circle-down"></span>
        </a>
      </div>
      <div class="navbar-dropdown-notifications__body navbar-dropdown-notifications__body-messages js-scrollable">
        <div class="navbar-dropdown-notifications__item is-unread">
          <img class="navbar-dropdown-notifications__item-avatar rounded-circle" src="/theme/img/users/user-4.png" alt="" width="40" height="40"/>
          <div class="navbar-dropdown-notifications__item-notify">
            <div>
              <span class="icon ua-icon-reply-to"></span>
              <strong>Gabriel Saunders</strong> replied to <strong>your comment</strong> to <strong>The secret weapon of lebound marketing...</strong>
            </div>
            <div class="navbar-dropdown-notifications__item-datetime">5 minute ago</div>
          </div>
          <div class="navbar-dropdown-notifications__item-actions">
            <span class="icon ua-icon-circle-check navbar-dropdown-notifications__item-mark-as-read" data-toggle="tooltip" data-placement="top" title="Mark as read"></span>
            <span class="icon ua-icon-circle-close navbar-dropdown-notifications__item-remove" data-toggle="tooltip" data-placement="top" title="Delete notification"></span>
          </div>
        </div>
        <div class="navbar-dropdown-notifications__item">
          <img class="navbar-dropdown-notifications__item-avatar rounded-circle" src="/theme/img/users/user-5.png" alt="" width="40" height="40"/>
          <div class="navbar-dropdown-notifications__item-notify">
            <div>
              <span class="icon ua-icon-comments"></span>
              <strong>Gabriel Saunders</strong> replied to <strong>your comment</strong> to <strong>The secret weapon of lebound marketing...</strong>
            </div>
            <div class="navbar-dropdown-notifications__item-datetime">5 minute ago</div>
          </div>
          <div class="navbar-dropdown-notifications__item-actions">
            <span class="icon ua-icon-circle-check navbar-dropdown-notifications__item-mark-as-read" data-toggle="tooltip" data-placement="top" title="Mark as read"></span>
            <span class="icon ua-icon-circle-close navbar-dropdown-notifications__item-remove" data-toggle="tooltip" data-placement="top" title="Delete notification"></span>
          </div>
        </div>
        <div class="navbar-dropdown-notifications__item is-unread">
          <img class="navbar-dropdown-notifications__item-avatar rounded-circle" src="/theme/img/users/user-6.png" alt="" width="40" height="40"/>
          <div class="navbar-dropdown-notifications__item-notify">
            <div>
              <span class="icon ua-icon-star"></span>
              <strong>Shawna Cohen</strong> replied to <strong>your comment</strong> to <strong>The secret weapon of lebound marketing...</strong>
            </div>
            <div class="navbar-dropdown-notifications__item-datetime">5 minute ago</div>
          </div>
          <div class="navbar-dropdown-notifications__item-actions">
            <span class="icon ua-icon-circle-check navbar-dropdown-notifications__item-mark-as-read" data-toggle="tooltip" data-placement="top" title="Mark as read"></span>
            <span class="icon ua-icon-circle-close navbar-dropdown-notifications__item-remove" data-toggle="tooltip" data-placement="top" title="Delete notification"></span>
          </div>
        </div>
        <div class="navbar-dropdown-notifications__item is-unread">
          <img class="navbar-dropdown-notifications__item-avatar rounded-circle" src="/theme/img/users/user-7.png" alt="" width="40" height="40"/>
          <div class="navbar-dropdown-notifications__item-notify">
            <div>
              <span class="icon ua-icon-download"></span>
              <strong>Jason Kendall</strong> replied to <strong>your comment</strong> to <strong>The secret weapon of lebound marketing...</strong>
            </div>
            <div class="navbar-dropdown-notifications__item-datetime">5 minute ago</div>
          </div>
          <div class="navbar-dropdown-notifications__item-actions">
            <span class="icon ua-icon-circle-check navbar-dropdown-notifications__item-mark-as-read" data-toggle="tooltip" data-placement="top" title="Mark as read"></span>
            <span class="icon ua-icon-circle-close navbar-dropdown-notifications__item-remove" data-toggle="tooltip" data-placement="top" title="Delete notification"></span>
          </div>
        </div>
      </div><a class="navbar-dropdown-notifications__view-all" href="#"><span class="icon ua-icon-view-all"></span><span>View all</span></a>
    </div>
  </div>
  <div class="dropdown navbar-dropdown no-arrow navbar-help-dropdown navbar-notify-dropdown--help">
    <a class="dropdown-toggle navbar-dropdown-toggle" data-toggle="dropdown" href="#">
      <span class="navbar-notify">
        <span>
          <span class="navbar-notify__icon ua-icon-help-circle"></span>
          <span class="navbar-notify__text">Info</span>
        </span>
      </span>
    </a>
    <div class="dropdown-menu dropdown-menu-center navbar-dropdown-menu">
      <h6 class="navbar-help-dropdown__heading">Need Help?</h6>
      <p class="navbar-help-dropdown__desc">
        Give us a call 888-898-8302 <br>
        send a email: <a href="#">info@example.com</a> <br>
        or
      </p>
      <div>
        <a href="help-center-submit-ticket.html" class="btn btn-info navbar-help-dropdown__submit">Submit a Ticket</a>
      </div>
    </div>
  </div>
  <div class="dropdown navbar-dropdown">
    <a class="dropdown-toggle navbar-dropdown-toggle navbar-dropdown-toggle__user" data-toggle="dropdown" href="#">
      <!-- <img src="/theme/img/users/user-3.png" alt="" class="navbar-dropdown-toggle__user-avatar"> -->
      <span class="navbar-dropdown__user-name">{{$currentUser->name}}</span>
    </a>
    <div class="dropdown-menu navbar-dropdown-menu navbar-dropdown-menu__user">
      <div class="navbar-dropdown-user-content">
        <div class="dropdown-user__avatar">
          <!-- <img src="/theme/img/users/user-3.png" alt=""/> -->
        </div>
        <div class="dropdown-info">
          <div class="dropdown-info__name">{{$currentUser->getName()}}</div>
          @if($currentUser->isOwner())
          <div class="dropdown-info__job">Manager</div>
          @endif
          <div class="dropdown-info-buttons">
            <a class="dropdown-info__viewprofile" href="#">View Profile</a>
            @if($currentUser->isOwner())
            <a class="dropdown-info__addaccount" href="#">Add account</a>
            @endif
          </div>
        </div>
      </div>
      <a class="dropdown-item navbar-dropdown__item" href="#">Upgrade to <span>PRO</span></a>
      <a class="dropdown-item navbar-dropdown__item" href="#">Invite team member</a>
      <a class="dropdown-item navbar-dropdown__item" href="#">Fedback</a>
      <a class="dropdown-item navbar-dropdown__item" href="#">Help</a>
      <a class="dropdown-item navbar-dropdown__item" href="{{ route('logout') }}"
                                                  onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                                                  Sign Out</a>
    </div>
  </div>
</div>
</div>


<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
