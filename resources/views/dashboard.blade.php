@extends('layouts.theme')

@section('content')

<div class="container-fluid">

  <div class="page-content__header">
    <div>
      <h2 class="page-content__header-heading">Dashboard</h2>
      <div class="page-content__header-description">Welcome to Recruthor Intelligent Dashboard</div>
    </div>
    <div class="page-content__header-meta">
      <a href="#" class="btn btn-info icon-left">
        Add Widget <span class="btn-icon ua-icon-plus-circle"></span>
      </a>
    </div>
  </div>



  <div class="row">
    <div class="col-xl-3 col-lg-3 col-md-6">
      <div class="widget widget-alpha widget-alpha--color-amaranth">
        @if(0 == $projectsCount)
        <div>
          <div class="widget-alpha__amount">No projects</div>
          <div class="widget-alpha__description">
            <a href="{{route('projects/add')}}" class="btn btn-success btn-sm btn-block"> Add your first </a>
          </div>
        </div>
        @else
        <div>
          <div class="widget-alpha__amount">{{ $projectsCount }}</div>
          <div class="widget-alpha__description">Total Projects</div>
        </div>
        @endif
        <span class="widget-alpha__icon ua-icon-suitcase"></span>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-6">
      <div class="widget widget-alpha widget-alpha--color-green-jungle">
        @if(0 == $candidatesCount)
        <div>
            <div class="widget-alpha__amount">No Candidates</div>
            <div class="widget-alpha__description">
                <a href="{{route('candidates/add')}}" class="btn btn-success btn-sm btn-block"> Add your first </a>
            </div>
          </div>
        <span class="widget-alpha__icon ua-icon-user-outline"></span>
        @else
        <div>
            <div class="widget-alpha__amount">{{ $candidatesCount }}</div>
            <div class="widget-alpha__description">Total Candidates</div>
          </div>
        <span class="widget-alpha__icon ua-icon-user-outline"></span>
        @endif
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-6">
      <div class="widget widget-alpha widget-alpha--color-orange widget-alpha--donut">
        <div>
          <div class="widget-alpha__amount">64,87%</div>
          <div class="widget-alpha__description">Conversion Rate</div>
        </div>
        <span class="widget-alpha__icon ua-icon-conversion-rate"></span>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-6">
      <div class="widget widget-alpha widget-alpha--color-java widget-alpha--help">
        <div>
          <div class="widget-alpha__amount"><font color="red">22</font> / 425</div>
          <div class="widget-alpha__description">Tasks Resolved</div>
        </div>
        <span class="widget-alpha__icon ua-icon-tasks"></span>
      </div>
    </div>
  </div>

  <!-- <div class="row">
    <div class="col-lg-3 col-md-6">
      <div class="widget widget-beta widget-beta--green">
        <div class="widget-beta__body">
          <div class="widget-beta__heading">
            <span class="ua-icon-users-group widget-beta__heading-icon"></span> New Clients
          </div>
          <div class="widget-beta__amount">459</div>
          <div class="widget-beta__desc"><span class="ua-icon-chart-arrow-up widget-beta__desc-icon"></span> 15% from yesterday</div>
        </div>
        <div class="widget-beta__chart">
          <div id="sparkline-chart-new-clients" class="widget-beta__chart-container"></div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="widget widget-beta widget-beta--seance">
        <div class="widget-beta__body">
          <div class="widget-beta__heading">
            <span class="ua-icon-wallet widget-beta__heading-icon"></span> Total Sales
          </div>
          <div class="widget-beta__amount">$8990.63</div>
          <div class="widget-beta__desc"><span class="ua-icon-chart-arrow-up widget-beta__desc-icon"></span> 70% last month</div>
        </div>
        <div class="widget-beta__chart">
          <div id="sparkline-chart-total-sales" class="widget-beta__chart-container"></div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="widget widget-beta widget-beta--lynch">
        <div class="widget-beta__body">
          <div class="widget-beta__heading">
            <span class="ua-icon-rating-up widget-beta__heading-icon"></span> Total Sales
          </div>
          <div class="widget-beta__amount">$806,52</div>
          <div class="widget-beta__desc"><span class="ua-icon-chart-arrow-up widget-beta__desc-icon"></span> 80% from yesterday</div>
        </div>
        <div class="widget-beta__chart">
          <div id="sparkline-chart-total-sales2" class="widget-beta__chart-container"></div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="widget widget-beta widget-beta--purple">
        <div class="widget-beta__body">
          <div class="widget-beta__heading">
            <span class="ua-icon-blank-document widget-beta__heading-icon"></span> New Invoice
          </div>
          <div class="widget-beta__amount">1806</div>
          <div class="widget-beta__desc"><span class="ua-icon-chart-arrow-down widget-beta__desc-icon"></span> 3% from last month</div>
        </div>
        <div class="widget-beta__chart">
          <div id="sparkline-chart-new-invoices" class="widget-beta__chart-container"></div>
        </div>
      </div>
    </div>
  </div> -->

  <div class="row">
    <div class="col-xl-6 col-lg-6">

    </div>
    <div class="col-xl-6 col-lg-6">
      <div class="widget widget-controls widget-table widget-notifications">
            <div class="widget-controls__header widget-controls__header--bordered">
              <div>
                <span class="widget-controls__header-icon ua-icon-widget-notifications"></span>
                Latest activities
              </div>
            </div>
            <div class="widget-controls__content js-scrollable" data-simplebar="init"><div class="simplebar-track vertical" style="visibility: visible;"><div class="simplebar-scrollbar" style="top: 2px; height: 319px;"></div></div><div class="simplebar-track horizontal" style="visibility: hidden;"><div class="simplebar-scrollbar"></div></div><div class="simplebar-scroll-content" style="padding-right: 15px; margin-bottom: -30px;"><div class="simplebar-content" style="padding-bottom: 15px; margin-right: -15px;">
              <div class="widget-notifications__items">
                @foreach($latestActivity as $activity)
                <div class="m-social-profile__activity-item">

                  <div class="m-social-profile__activity-item-header">
                    <img src="/theme/img/users/user-15.png" alt="" class="m-social-profile__activity-item-avatar rounded-circle" width="56" height="56">
                    <div class="m-social-profile__activity-item-info">
                      <div>
                        {!! $activity->getActivityText() !!}
                      </div>
                      <div>
                        <span class="m-social-profile__activity-item-date">
                          <span class="ua-icon-datepicker m-social-profile__activity-item-icon"></span> <span>{{ $activity->getActivityTime() }}</span>
                        </span>
                        <span class="m-social-profile__activity-item-location">
                          <!-- <span class="ua-icon-map m-social-profile__activity-item-icon"></span> <span></span> -->
                        </span>
                      </div>
                    </div>

                  </div>
                </div>
                @endforeach
              </div>
            </div></div></div>
            <!-- <div class="widget-controls__footer">
              <a href="#" class="widget-controls__footer-view-all">Show previos week</a>
            </div> -->
          </div>
          </div>
  </div>

</div>
@endsection
