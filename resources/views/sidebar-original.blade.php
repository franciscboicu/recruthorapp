<div class="sidebar-section">
  <div class="sidebar-section__scroll">
    <div class="sidebar-section__user has-background">
      <img src="/theme/img/users/user-19.png" alt="" class="sidebar-section__user-avatar rounded-circle">

      <div class="dropdown sidebar-section__user-dropdown">
        <a class="dropdown-toggle sidebar-section__user-dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{ $currentUser->name }}
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <a class="dropdown-item" href="#">Settings</a>
          <a class="dropdown-item" href="#">Help</a>
          <a class="dropdown-item" href="#">Sign Out</a>
        </div>
      </div>
    </div>
    <div>
      <!-- <div class="sidebar-section__separator">Workflow</div> -->
      <ul class="sidebar-section-nav">

        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link" href="{{ route('dashboard') }}">
            <span class="sidebar-section-nav__item-icon ua-icon-home"></span>
            <span class="sidebar-section-nav__item-text">Dashboard</span>
          </a>
        </li>

        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link" href="{{ route('candidates/browse') }}">
            <span class="sidebar-section-nav__item-icon ua-icon-user"></span>
            <span class="sidebar-section-nav__item-text">Candidates</span>
          </a>
        </li>

        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link" href="{{ route('projects/browse') }}">
            <span class="sidebar-section-nav__item-icon ua-icon-table-grid"></span>
            <span class="sidebar-section-nav__item-text">Projects</span>
          </a>
        </li>

        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link" href="datatables.html">
            <span class="sidebar-section-nav__item-icon ua-icon-tasks"></span>
            <span class="sidebar-section-nav__item-text">Tasks</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-section-nav">
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-home"></span>
            <span class="sidebar-section-nav__item-text">Dashboard</span>
            <span class="badge sidebar-section-nav__badge">2</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="index.html">Default Dashboard</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="booking-dashboard.html">Booking Dashboard</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="medical-dashboard.html">Medical Dashboard</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="sales-dashboard.html">Sales Dashboard</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="payment-dashboard.html">Payment Dashboard</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-layout"></span>
            <span class="sidebar-section-nav__item-text">Page Layouts</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="sidebar-md.html">Medium Sidebar</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="container-md.html">Medium Container</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="boxed-layout.html">Boxed Layout</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="full-height-content.html">Full Height Content</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="menu-and-content.html">Menu and Content</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="sidebar-a.html">Sidebar (a)</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="sidebar-b.html">Sidebar (b)</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="ecommerce-navbar.html">E-commerce Navbar</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="simple-navbar.html">Simple Navbar</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-ui"></span>
            <span class="sidebar-section-nav__item-text">UI Features</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="alerts.html">Alerts</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="breadcrumbs.html">Breadcrumbs</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="badges.html">Badges</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="collapse.html">Collapse</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="colors.html">Colors</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="pagination.html">Pagination</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="progress.html">Progress</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="modal.html">Modal</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="tables.html">Tables</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="tabs.html">Tabs</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="tooltips-and-popovers.html">Tooltips &amp; Popovers</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="typography.html">Typography</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-grid"></span>
            <span class="sidebar-section-nav__item-text">Widgets</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="widgets-cards.html">Card Widgets</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="widgets-social.html">Social Widgets</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="widgets-ecommerce.html">E-commerce Widgets</a></li>
          </ul>
        </li>
      </ul>

      <div class="sidebar-section__separator">Form</div>

      <ul class="sidebar-section-nav">
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-input"></span>
            <span class="sidebar-section-nav__item-text">Basic Elements</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="inputs.html">Inputs</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="input-group.html">Input Group</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="buttons.html">Buttons</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="checkbox-radio.html">Checkbox &amp; Radio</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="select.html">Select</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="form-mask-input.html">Mask Input</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-input"></span>
            <span class="sidebar-section-nav__item-text">Form Wizard</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="form-wizard.html">Form Wizard</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="form-wizard-a.html">Form Wizard (a)</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="form-wizard-b.html">Form Wizard (b)</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="form-wizard-c.html">Form Wizard (c)</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="form-wizard-d.html">Form Wizard (d)</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="form-wizard-e.html">Form Wizard (e)</a></li>
          </ul>
        </li>
      </ul>

      <div class="sidebar-section__separator">Features</div>

      <ul class="sidebar-section-nav">
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-component"></span>
            <span class="sidebar-section-nav__item-text">Components</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="range-slider.html">Range Slider</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="confirm-alerts.html">Confirm Alerts</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="tag-editor.html">Tag Editor</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="datepicker.html">Date Picker</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="date-range-picker.html">Date Range Picker</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="file-upload.html">File Upload</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="growl-notifications.html">Growl Notifications</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="wysiwyg-editor.html">Wysiwyg Editor</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-pages"></span>
            <span class="sidebar-section-nav__item-text">Pages</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="signin.html">Sign In</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="signin-a.html">Sign In (a)</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="signup.html">Sign Up</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="signup-a.html">Sign Up (a)</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="403.html">Page 403</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="404.html">Page 404</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="500.html">Page 500</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="invoices.html">Invoices</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="pricing.html">Pricing</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="subscribe-plans.html">Subscribe Plans</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="pricing-and-plans.html">Pricing and Plans</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="documents.html">Documents</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="trial-period-expired.html">Trial Expired</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-pages"></span>
            <span class="sidebar-section-nav__item-text">Extra Pages</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="contact.html">Contact</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="users-grid.html">Users Grid</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="users-contacts.html">Users Contacts</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="activity.html">Activity</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="invoices-datagrid.html">Invoices (DataTable)</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="invoice-overview.html">Invoice Overview</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="tree-view.html">Tree View</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="timeline.html">Timeline</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="listings-travel.html">Travel Listing</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-help-circle"></span>
            <span class="sidebar-section-nav__item-text">Help Center</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="help-center.html">Browse Help Desk</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="help-center-submit-ticket.html">Submit Ticket</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-settings"></span>
            <span class="sidebar-section-nav__item-text">Settings</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="app-settings.html">App Settings</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="global-settings.html">Global Settings</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="account-settings.html">Account Settings</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link" href="email-templates.html">
            <span class="sidebar-section-nav__item-icon ua-icon-envelope"></span>
            <span class="sidebar-section-nav__item-text">Email Templates</span>
          </a>
        </li>
      </ul>




    </div>



  </div>


</div>
