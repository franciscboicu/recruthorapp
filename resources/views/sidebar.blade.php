<div class="sidebar-section">
  <div class="sidebar-section__scroll" data-simplebar="init">
    <div class="simplebar-track vertical" style="visibility: visible;">
      <div class="simplebar-scrollbar" style="top: 2px; height: 383px;"></div>
    </div>
    <div class="simplebar-track horizontal" style="visibility: hidden;">
      <div class="simplebar-scrollbar"></div>
    </div>
    <div class="simplebar-scroll-content" style="padding-right: 15px; margin-bottom: -30px;">
    <div class="simplebar-content" style="padding-bottom: 15px; margin-right: -15px;">
    
      <div class="sidebar-section__user has-background">
        <img src="/theme/img/users/user-19.png" alt="" class="sidebar-section__user-avatar rounded-circle">
  
        <div class="dropdown sidebar-section__user-dropdown">
          <a class="dropdown-toggle sidebar-section__user-dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{$currentUser->getName()}}
          </a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 22px, 0px); top: 0px; left: 0px; will-change: transform;">
            <a class="dropdown-item" href="#">Settings</a>
            <a class="dropdown-item" href="#">Help</a>
            {{-- <a class="dropdown-item" href="#">Sign Out</a> --}}
          </div>
        </div>
      </div>

    <div style="min-height:550px;" class="mt-0">
      {{-- <div class="sidebar-section__dropdown">

        <div class="dropdown sidebar-section__dropdown-item">
          <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
            Client: Company A
          </a>
          <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 22px, 0px); top: 0px; left: 0px; will-change: transform;">
            <a class="dropdown-item" href="#">Company A</a>
            <a class="dropdown-item" href="#">Company B</a>
            <a class="dropdown-item" href="#">Company C</a>
          </div>
        </div>
      </div> --}}

      {{-- <div class="sidebar-section__invite-block">
        <div class="sidebar-section__invite-users">
          <a href="#" class="sidebar-section__invite-user">
            <img src="/theme/img/users/user-15.png" alt="" class="sidebar-section__invite-user-avatar rounded-circle" width="34" height="34">
          </a>
          <a href="#" class="sidebar-section__invite-user">
            <img src="/theme/img/users/user-16.png" alt="" class="sidebar-section__invite-user-avatar rounded-circle" width="34" height="34">
          </a>
          <a href="#" class="sidebar-section__invite-user">
            <img src="/theme/img/users/user-17.png" alt="" class="sidebar-section__invite-user-avatar rounded-circle" width="34" height="34">
          </a>
          <span class="sidebar-section__invite-plus">+ 3 people</span>
        </div>
        <a href="#" class="sidebar-section__invite-link">Invite</a>
      </div> --}}

      <div class="sidebar-section__separator">WORKFLOW</div>
      <ul class="sidebar-section-nav">
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link" href="{{route('dashboard')}}">
            <span class="sidebar-section-nav__item-icon ua-icon-home"></span>
            <span class="sidebar-section-nav__item-text">Dashboard</span>
            <!-- <span class="badge sidebar-section-nav__badge">3</span> -->
          </a>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link" href="{{route('candidates/browse')}}">
            <span class="sidebar-section-nav__item-icon ua-icon-layout"></span>
            <span class="sidebar-section-nav__item-text">Candidates</span>
            <!-- <span class="badge sidebar-section-nav__badge">2</span> -->
          </a>
        </li>
        <!-- <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link" href="#">
            <span class="sidebar-section-nav__item-icon ua-icon-layout"></span>
            <span class="sidebar-section-nav__item-text">Page Layouts</span>
            <span class="badge sidebar-section-nav__badge">2</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="sidebar-section-md.html">Medium sidebar-section</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="container-md.html">Medium Container</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="boxed-layout.html">Boxed Layout</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="full-height-content.html">Full Height Content</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="menu-and-content.html">Menu and Content</a></li>
          </ul>
        </li> -->
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link" href="{{route('projects/browse')}}">
            <span class="sidebar-section-nav__item-icon ua-icon-ui"></span>
            <span class="sidebar-section-nav__item-text">Projects</span>
          </a>
        </li>
      </ul>


      <div class="sidebar-section__separator">Account</div>

      <ul class="sidebar-section-nav">
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-text">Dashboard</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="index.html">Default Dashboard</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="medical-dashboard.html">Medical Dashboard</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="sales-dashboard.html">Sales Dashboard</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="payment-dashboard.html">Payment Dashboard</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-text">Page Layouts</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="sidebar-section-md.html">Medium sidebar-section</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="container-md.html">Medium Container</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="boxed-layout.html">Boxed Layout</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="full-height-content.html">Full Height Content</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="menu-and-content.html">Menu and Content</a></li>
          </ul>
        </li>
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-text">UI Features</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="alerts.html">Alerts</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="breadcrumbs.html">Breadcrumbs</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="badges.html">Badges</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="collapse.html">Collapse</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="colors.html">Colors</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="pagination.html">Pagination</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="progress.html">Progress</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="modal.html">Modal</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="tables.html">Tables</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="tabs.html">Tabs</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="tooltips.html">Tooltips</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="typography.html">Typography</a></li>
          </ul>
        </li>
      </ul>



      
    </div>

    <div class="sidebar-section-nav__footer">
      <ul class="sidebar-section-nav">
        <li class="sidebar-section-nav__item">
          <a class="sidebar-section-nav__link sidebar-section-nav__link-dropdown" href="#">
            <span class="sidebar-section-nav__item-text">Settings</span>
          </a>
          <ul class="sidebar-section-subnav">
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="">My Account</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="">Notifications</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="">Partners</a></li>
            <li class="sidebar-section-subnav__item"><a class="sidebar-section-subnav__link" href="">Billing</a></li>
          </ul>
        </li>

        <li class="sidebar-section-nav__item sidebar-section-nav__item-btn mb-4">
          <a href="#" class="btn btn-info btn-block">Create project</a>
        </li>
        <li class="sidebar-section-nav__item sidebar-section-nav__item-btn mb-4">
          <div class="navbar__menu d-flex justify-content-end">
              <a href="#" class="btn btn-danger btn-block btn-xs navbar__btn-sm">Upgrade to Premium Plan!</a>
          </div>
        </li>

        <li class="">
          <!-- major.minor[.build[.revision]]  (example: 1.2.12.102) -->

          <span class="sidebar-section-nav__link sidebar-section-nav__source" href="#">v1.0.0.1</span>
        </li>
      </ul>
    </div>
  </div></div></div>
</div>
